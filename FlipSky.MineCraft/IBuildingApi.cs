﻿namespace FlipSky.MineCraft
{
    /// <summary>
    /// The general building API
    /// </summary>
    public interface IBuildingApi
    {
        /// <summary>
        /// Get the block at the given location
        /// </summary>
        int GetBlock(Location location);

        /// <summary>
        /// Set the block at the given location
        /// </summary>
        void SetBlock(Location location, int block);

        /// <summary>
        /// Get the height of the world at the given x, y location
        /// </summary>
        Location GetHeight(Location location);

        /// <summary>
        /// Spawn a new entity
        /// </summary>
        int SpawnEntity(Location location, int entityId);
    }
}
