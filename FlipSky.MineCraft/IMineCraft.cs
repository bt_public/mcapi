﻿using System.Collections.Generic;

namespace FlipSky.MineCraft
{
    /// <summary>
    /// Base API entry point
    /// </summary>
    public interface IMineCraft : IBuildingApi
    {
        #region API Groups
        /// <summary>
        /// Access to the player
        /// </summary>
        IEntityApi Player { get; }

        /// <summary>
        /// Access to events API
        /// </summary>
        IEventsApi Events { get; }
        #endregion

        #region World Level API
        /// <summary>
        /// Get the entity id's for all players
        /// </summary>
        IEnumerable<int> GetPlayerEntityIds();

        /// <summary>
        /// Get the entity ID of the player with the specified name
        /// </summary>
        bool GetPlayerEntityId(string name, out int entityId);

        /// <summary>
        /// Post a message to the chat system
        /// </summary>
        void PostMessage(string message);

        /// <summary>
        /// Get the API to work with a specific entity
        /// </summary>
        IEntityApi GetEntity(int entityId);
        #endregion

        #region Connection Control
        /// <summary>
        /// Determine if we have a connection or not
        /// </summary>
        bool Connected { get; }

        /// <summary>
        /// Disconnect from the server
        /// </summary>
        void Disconnect();
        #endregion
    }
}
