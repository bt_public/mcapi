﻿using System.Collections.Generic;

namespace FlipSky.MineCraft
{
    /// <summary>
    /// API to access events
    /// </summary>
    public interface IEventsApi
    {
        /// <summary>
        /// Get all chat events that have occurred since the last call
        /// </summary>
        IEnumerable<IChatEvent> GetChatEvents();

        /// <summary>
        /// Get all hit events that have occurred since the last call
        /// </summary>
        IEnumerable<IHitEvent> GetHitEvents();

        /// <summary>
        /// Flush all events since the last check
        /// </summary>
        void FlushEvents();
    }
}
