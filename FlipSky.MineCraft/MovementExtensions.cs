using System;

namespace FlipSky.MineCraft
{
    /// <summary>
    /// Helpers to manage movement
    /// </summary>
    public static class MovementExtensions
    {
        //--- Transforms and lookup tables
        private static readonly Transform LocalToWorldTransform = Transform.RotateX(90);
        private static readonly Transform WorldToLocalTransform = Transform.RotateX(-90);

        private static readonly Location[] Directions = new Location[] {
            new Location(0, 1, 0), // NORTH
            new Location(1, 0, 0),  // EAST
            new Location(0, -1, 0),  // SOUTH
            new Location(-1, 0, 0)  // WEST
        };

        /// <summary>
        /// Transform a single location from local to world co-ordinates
        /// </summary>
        public static Location LocalToWorld(this Location location)
        {
            return LocalToWorldTransform.Apply(location);
        }

        /// <summary>
        /// Transform a single location from world to local co-ordinates
        /// </summary>
        public static Location WorldToLocal(this Location location)
        {
            return WorldToLocalTransform.Apply(location);
        }

        /// <summary>
        /// Move the specified number of steps in the given heading. A negative
        /// value indicates backward movement.
        /// </summary>
        public static Location Move(this Location location, Heading heading, int steps)
        {
            int offset = (int)heading;
            return new Location(
                location.X + (steps * Directions[offset].X),
                location.Y + (steps * Directions[offset].Y),
                location.Z + (steps * Directions[offset].Z)
                );
        }

        /// <summary>
        /// Translate the location
        /// </summary>
        public static Location Translate(this Location location, int dx, int dy, int dz)
        {
            return new Location(
                location.X + dx,
                location.Y + dy,
                location.Z + dz
                );
        }

        /// <summary>
        /// Move vertically the specified number steps. A negative number
        /// indicates downward movement.
        /// </summary>
        public static Location MoveUp(this Location location, int steps)
        {
            return new Location(location.X, location.Y, location.Z + steps);
        }

        /// <summary>
        /// Rotate the heading clockwise the given number of steps. A negative
        /// value indicates counter-clockwise movement.
        /// </summary>
        public static Heading Rotate(this Heading heading, int steps)
        {
            int offset = (int)heading + steps;
            while (offset < 0)
                offset = offset + 4; // Is there a better way?
            offset = offset % 4;
            return (Heading)Enum.ToObject(typeof(Heading), offset);
        }
    }
}