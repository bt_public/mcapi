﻿namespace FlipSky.MineCraft
{
    /// <summary>
    /// Represents an event from a hit (with sword)
    /// </summary>
    public interface IHitEvent
    {
        /// <summary>
        /// Type of hit event
        /// </summary>
        int Type { get;  }

        /// <summary>
        /// Location of hit event
        /// </summary>
        Location Location { get; }

        /// <summary>
        /// Face???
        /// </summary>
        int Face { get; }

        /// <summary>
        /// Entity that performed the hit
        /// </summary>
        int EntityId { get; }
    }
}
