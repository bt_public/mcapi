﻿using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace FlipSky.MineCraft
{
    /// <summary>
    /// Define the known entities
    /// </summary>
    public static class Entities
    {
        //--- Well known entities
        public const int EXPERIENCE_ORB = 2;
        public const int AREA_EFFECT_CLOUD = 3;
        public const int ELDER_GUARDIAN = 4;
        public const int WITHER_SKELETON = 5;
        public const int STRAY = 6;
        public const int EGG = 7;
        public const int LEASH_HITCH = 8;
        public const int PAINTING = 9;
        public const int ARROW = 10;
        public const int SNOWBALL = 11;
        public const int FIREBALL = 12;
        public const int SMALL_FIREBALL = 13;
        public const int ENDER_PEARL = 14;
        public const int ENDER_SIGNAL = 15;
        public const int THROWN_EXP_BOTTLE = 17;
        public const int ITEM_FRAME = 18;
        public const int WITHER_SKULL = 19;
        public const int PRIMED_TNT = 20;
        public const int HUSK = 23;
        public const int SPECTRAL_ARROW = 24;
        public const int SHULKER_BULLET = 25;
        public const int DRAGON_FIREBALL = 26;
        public const int ZOMBIE_VILLAGER = 27;
        public const int SKELETON_HORSE = 28;
        public const int ZOMBIE_HORSE = 29;
        public const int ARMOR_STAND = 30;
        public const int DONKEY = 31;
        public const int MULE = 32;
        public const int EVOKER_FANGS = 33;
        public const int EVOKER = 34;
        public const int VEX = 35;
        public const int VINDICATOR = 36;
        public const int ILLUSIONER = 37;
        public const int MINECART_COMMAND = 40;
        public const int BOAT = 41;
        public const int MINECART = 42;
        public const int MINECART_CHEST = 43;
        public const int MINECART_FURNACE = 44;
        public const int MINECART_TNT = 45;
        public const int MINECART_HOPPER = 46;
        public const int MINECART_MOB_SPAWNER = 47;
        public const int CREEPER = 50;
        public const int SKELETON = 51;
        public const int SPIDER = 52;
        public const int GIANT = 53;
        public const int ZOMBIE = 54;
        public const int SLIME = 55;
        public const int GHAST = 56;
        public const int PIG_ZOMBIE = 57;
        public const int ENDERMAN = 58;
        public const int CAVE_SPIDER = 59;
        public const int SILVERFISH = 60;
        public const int BLAZE = 61;
        public const int MAGMA_CUBE = 62;
        public const int ENDER_DRAGON = 63;
        public const int WITHER = 64;
        public const int BAT = 65;
        public const int WITCH = 66;
        public const int ENDERMITE = 67;
        public const int GUARDIAN = 68;
        public const int SHULKER = 69;
        public const int PIG = 90;
        public const int SHEEP = 91;
        public const int COW = 92;
        public const int CHICKEN = 93;
        public const int SQUID = 94;
        public const int WOLF = 95;
        public const int MUSHROOM_COW = 96;
        public const int SNOWMAN = 97;
        public const int OCELOT = 98;
        public const int IRON_GOLEM = 99;
        public const int HORSE = 100;
        public const int RABBIT = 101;
        public const int POLAR_BEAR = 102;
        public const int LLAMA = 103;
        public const int LLAMA_SPIT = 104;
        public const int PARROT = 105;
        public const int VILLAGER = 120;
        public const int ENDER_CRYSTAL = 200;

        #region Helpers
        // The actual mapping of names to values
        private static Dictionary<string, int> _names;

        /// <summary>
        /// Name mapping property, built once on first reference
        /// </summary>
        private static Dictionary<string, int> BlockNames
        {
            get
            {
                lock (typeof(Entities))
                {
                    if (_names == null)
                    {
                        // Build the mapping using reflection
                        FieldInfo[] fields = typeof(Entities).GetFields(BindingFlags.Public | BindingFlags.Static);
                        _names = fields.Where(f => f.IsLiteral && f.FieldType == typeof(int)).ToDictionary(k => k.Name.ToLower(), v => (int)v.GetRawConstantValue());
                    }
                    return _names;
                }
            }
        }

        /// <summary>
        /// Try and turn a block name into an integer constant (case insensitive)
        /// </summary>
        public static bool TryParse(string name, out int value)
        {
            return BlockNames.TryGetValue(name.ToLower(), out value);
        }
        #endregion
    }
}
