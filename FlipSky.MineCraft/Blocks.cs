using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace FlipSky.MineCraft 
{
    /// <summary>
    /// Block identifiers
    /// </summary>
    public static class Blocks 
    {
        //--- Well known blocks
        public const int NONE = -1;
        public const int AIR = 0;
        public const int STONE = 1;
        public const int GRASS = 2;
        public const int DIRT = 3;
        public const int COBBLESTONE = 4;
        public const int WOOD_PLANKS = 5;
        public const int SAPLING = 6;
        public const int BEDROCK = 7;
        public const int WATER_FLOWING = 8;
        public const int WATER = WATER_FLOWING;
        public const int WATER_STATIONARY = 9;
        public const int LAVA_FLOWING = 10;
        public const int LAVA = LAVA_FLOWING;
        public const int LAVA_STATIONARY = 11;
        public const int SAND = 12;
        public const int GRAVEL = 13;
        public const int GOLD_ORE = 14;
        public const int IRON_ORE = 15;
        public const int COAL_ORE = 16;
        public const int WOOD = 17;
        public const int LEAVES = 18;
        public const int GLASS = 20;
        public const int LAPIS_LAZULI_ORE = 21;
        public const int LAPIS_LAZULI_BLOCK = 22;
        public const int SANDSTONE = 24;
        public const int BED = 26;
        public const int RAIL_POWERED = 27;
        public const int RAIL_DETECTOR = 28;
        public const int COBWEB = 30;
        public const int GRASS_TALL = 31;
        public const int DEAD_BUSH = 32;
        public const int WOOL = 35;
        public const int FLOWER_YELLOW = 37;
        public const int FLOWER_CYAN = 38;
        public const int MUSHROOM_BROWN = 39;
        public const int MUSHROOM_RED = 40;
        public const int GOLD_BLOCK = 41;
        public const int IRON_BLOCK = 42;
        public const int STONE_SLAB_DOUBLE = 43;
        public const int STONE_SLAB = 44;
        public const int BRICK_BLOCK = 45;
        public const int TNT = 46;
        public const int BOOKSHELF = 47;
        public const int MOSS_STONE = 48;
        public const int OBSIDIAN = 49;
        public const int TORCH = 50;
        public const int FIRE = 51;
        public const int STAIRS_WOOD = 53;
        public const int CHEST = 54;
        public const int DIAMOND_ORE = 56;
        public const int DIAMOND_BLOCK = 57;
        public const int CRAFTING_TABLE = 58;
        public const int FARMLAND = 60;
        public const int FURNACE_INACTIVE = 61;
        public const int FURNACE_ACTIVE = 62;
        public const int SIGN_STANDING = 63;
        public const int DOOR_WOOD = 64;
        public const int LADDER = 65;
        public const int RAIL = 66;
        public const int STAIRS_COBBLESTONE = 67;
        public const int SIGN_WALL = 68;
        public const int DOOR_IRON = 71;
        public const int REDSTONE_ORE = 73;
        public const int TORCH_REDSTONE = 76;
        public const int SNOW = 78;
        public const int ICE = 79;
        public const int SNOW_BLOCK = 80;
        public const int CACTUS = 81;
        public const int CLAY = 82;
        public const int SUGAR_CANE = 83;
        public const int FENCE = 85;
        public const int PUMPKIN = 86;
        public const int NETHERRACK = 87;
        public const int SOUL_SAND = 88;
        public const int GLOWSTONE_BLOCK = 89;
        public const int LIT_PUMPKIN = 91;
        public const int STAINED_GLASS = 95;
        public const int BEDROCK_INVISIBLE = 95;
        public const int TRAPDOOR = 96;
        public const int STONE_BRICK = 98;
        public const int GLASS_PANE = 102;
        public const int MELON = 103;
        public const int FENCE_GATE = 107;
        public const int STAIRS_BRICK = 108;
        public const int STAIRS_STONE_BRICK = 109;
        public const int MYCELIUM = 110;
        public const int NETHER_BRICK = 112;
        public const int FENCE_NETHER_BRICK = 113;
        public const int STAIRS_NETHER_BRICK = 114;
        public const int END_STONE = 121;
        public const int WOODEN_SLAB = 126;
        public const int STAIRS_SANDSTONE = 128;
        public const int EMERALD_ORE = 129;
        public const int RAIL_ACTIVATOR = 157;
        public const int LEAVES2 = 161;
        public const int TRAPDOOR_IRON = 167;
        public const int FENCE_SPRUCE = 188;
        public const int FENCE_BIRCH = 189;
        public const int FENCE_JUNGLE = 190;
        public const int FENCE_DARK_OAK = 191;
        public const int FENCE_ACACIA = 192;
        public const int DOOR_SPRUCE = 193;
        public const int DOOR_BIRCH = 194;
        public const int DOOR_JUNGLE = 195;
        public const int DOOR_ACACIA = 196;
        public const int DOOR_DARK_OAK = 197;
        public const int GLOWING_OBSIDIAN = 246;
        public const int NETHER_REACTOR_CORE = 247;

        #region Helpers
        // The actual mapping of names to values
        private static Dictionary<string, int> _names;

        /// <summary>
        /// Name mapping property, built once on first reference
        /// </summary>
        private static Dictionary<string, int> BlockNames
        {
            get
            {
                lock (typeof(Blocks))
                {
                    if (_names == null)
                    {
                        // Build the mapping using reflection
                        FieldInfo[] fields = typeof(Blocks).GetFields(BindingFlags.Public | BindingFlags.Static);
                        _names = fields.Where(f => f.IsLiteral && f.FieldType == typeof(int)).ToDictionary(k => k.Name.ToLower(), v => (int)v.GetRawConstantValue());
                    }
                    return _names;
                }
            }
        }

        /// <summary>
        /// Try and turn a block name into an integer constant (case insensitive)
        /// </summary>
        public static bool TryParse(string name, out int value)
        {
            return BlockNames.TryGetValue(name.ToLower(), out value);
        }
        #endregion
    }
}