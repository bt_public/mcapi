namespace FlipSky.MineCraft
{
    /// <summary>
    /// Simple API for an entity
    /// </summary>
    public interface IEntityApi : IBuildingApi
    {
        /// <summary>
        /// Property version of name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// API call to get the name
        /// </summary>
        string GetName();

        /// <summary>
        /// Get the entities location
        /// </summary>
        Location GetLocation();

        /// <summary>
        /// Set the entities location
        /// </summary>
        void SetLocation(Location location);

        /// <summary>
        /// Get the entities heading
        /// </summary>
        Heading GetHeading();

        /// <summary>
        /// Get a transform to convert player local co-ordinates to
        /// world co-ordinates.
        /// </summary>
        Transform GetTransform();

        /// <summary>
        /// Post a message to the chat
        /// </summary>
        void PostMessage(string message);
    }
}