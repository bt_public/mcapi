﻿using System;

namespace FlipSky.MineCraft
{
    /// <summary>
    /// Represents a 3D transformation matrix
    /// </summary>
    public class Transform : Matrix
    {
        /// <summary>
        /// The identity matrix
        /// </summary>
        public static readonly Transform Identity = new Transform(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
            );

        /// <summary>
        /// Constructor
        /// </summary>
        private Transform(params double[] args) : base(4, 4, args) { }

        /// <summary>
        /// Apply another transform to this one and return the new transform.
        /// </summary>
        public Transform Apply(Transform other)
        {
            return new Transform(Multiply(this, other));
        }

        /// <summary>
        /// Apply to a Location instance and return the transformed location
        /// </summary>
        public Location Apply(Location location)
        {
            var result = Multiply(location, this);
            return new Location(
                (int)Math.Round(result[0] / result[3]),
                (int)Math.Round(result[1] / result[3]),
                (int)Math.Round(result[2] / result[3])
                );
        }

        /// <summary>
        /// Convert degrees to radians
        /// </summary>
        private static double Radians(int degrees)
        {
            return degrees * (Math.PI / 180.0);
        }

        #region Factories
        /// <summary>
        /// Create a transform that moves the points by the given offsets
        /// </summary>
        public static Transform Translate(int dx, int dy, int dz)
        {
            return new Transform(
                1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                dx, dy, dz, 1.0
                );
        }

        /// <summary>
        /// Create a transform that scales the points by the given amount
        /// </summary>
        public static Transform Scale(double sx, double sy, double sz)
        {
            return new Transform(
                sx, 0.0, 0.0, 0.0,
                0.0, sy, 0.0, 0.0,
                0.0, 0.0, sz, 0.0,
                0.0, 0.0, 0.0, 1.0
                );
        }

        /// <summary>
        /// Create a transform that rotates points around the X axis
        /// </summary>
        public static Transform RotateX(int angle)
        {
            var r = Radians(angle);
            return new Transform(
                1.0, 0.0, 0.0, 0.0,
                0.0, Math.Cos(r), -Math.Sin(r), 0.0,
                0.0, Math.Sin(r), Math.Cos(r), 0.0,
                0.0, 0.0, 0.0, 1.0
                );
        }

        /// <summary>
        /// Create a transform that rotates points around the Y axis
        /// </summary>
        public static Transform RotateY(int angle)
        {
            var r = -Radians(angle); // Right handed orientation
            return new Transform(
                Math.Cos(r), 0.0, Math.Sin(r), 0.0,
                0.0, 1.0, 0.0, 0.0,
                -Math.Sin(r), 0.0, Math.Cos(r), 0.0,
                0.0, 0.0, 0.0, 1.0
                );
        }

        /// <summary>
        /// Create a transform that rotates points around the Z axis
        /// </summary>
        public static Transform RotateZ(int angle)
        {
            var r = Radians(angle);
            return new Transform(
                Math.Cos(r), -Math.Sin(r), 0.0, 0.0,
                Math.Sin(r), Math.Cos(r), 0.0, 0.0,
                0.0, 0.0, 1.0, 0.0,
                0.0, 0.0, 0.0, 1.0
                );
        }
        #endregion
    }
}
