﻿namespace FlipSky.MineCraft
{
    /// <summary>
    /// Represents an event from a chat message
    /// </summary>
    public interface IChatEvent
    {
        /// <summary>
        /// EntityId of the user that posted the chat
        /// </summary>
        int EntityId { get; }

        /// <summary>
        /// Message body
        /// </summary>
        string Message { get; }
    }
}
