﻿using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// General API exception
    /// </summary>
    public class MineCraftApiException : MineCraftException
    {
        public MineCraftApiException() { }

        public MineCraftApiException(string message) : base(message) { }

        public MineCraftApiException(string message, Exception innerException) : base(message, innerException) { }

        protected MineCraftApiException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
