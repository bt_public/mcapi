﻿namespace FlipSky.MineCraft.API
{
    internal class ChatEvent : IChatEvent
    {
        /// <summary>
        /// EntityId that sent the message
        /// </summary>
        public int EntityId { get; private set; }

        /// <summary>
        /// Message body
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Decode from arguments
        /// </summary>
        public ChatEvent(string[] args)
        {
            EntityId = int.Parse(args[0]);
            Message = args[1];
        }
    }
}
