﻿using System;
using System.Linq;
using System.Collections.Generic;
using FlipSky.Logging;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// Implementation of the EntityApi for a specific entity
    /// </summary>
    internal class EntityApi : IEntityApi
    {
        //--- Rotation lookup
        static Dictionary<Heading, Transform> PlayerRotation = new Dictionary<Heading, Transform>()
        {
            { Heading.North, Transform.Identity },
            { Heading.East, Transform.RotateZ(90) },
            { Heading.South, Transform.RotateZ(180) },
            { Heading.West, Transform.RotateZ(270) }
        };

        //--- State
        private string _name;

        //--- Injections
        private readonly int? EntityId;
        private readonly MineCraftApi API;
        private readonly IProtocol Protocol;
        private readonly ILogger Log;

        /// <summary>
        /// Constructor with injections
        /// </summary>
        public EntityApi(MineCraftApi api, int? entityId, ILogger log)
        {
            EntityId = entityId;
            API = api;
            Protocol = api.Protocol;
            Log = log;
        }

        #region Helpers
        /// <summary>
        /// Adjust the command and set up the arguments
        /// </summary>
        private string GetCommandAndArgs(string command, out List<object> args)
        {
            args = new List<object>();
            if (!EntityId.HasValue)
                return $"player.{command}";
            args.Add(EntityId.Value);
            return $"entity.{command}";
        }
        #endregion

        #region Implementation of entity API
        /// <summary>
        /// Property version of name
        /// </summary>
        public string Name
        {
            get
            {
                if (_name == null)
                    _name = GetName();
                return _name;
            }
        }

        /// <summary>
        /// API call to get the name
        /// </summary>
        public string GetName()
        {
            if (!EntityId.HasValue)
                return "???";
            var cmd = GetCommandAndArgs("getName", out var args);
            return Protocol.SendReceive(cmd, args.Select(x => x.ToString()).ToArray());
        }

        /// <summary>
        /// Get the entities location (local co-ordinates)
        /// </summary>
        public Location GetLocation()
        {
            var cmd  = GetCommandAndArgs("getPos", out var args);
            var answer = Protocol.SendReceive(cmd, args.Select(x => x.ToString()).ToArray());
            var parts = answer.Split(',').Select(x => double.Parse(x)).ToList();
            return new Location((int)parts[0], (int)parts[1], (int)parts[2]).WorldToLocal();
        }

        /// <summary>
        /// Set the entities location (local co-ordinates)
        /// </summary>
        public void SetLocation(Location location)
        {
            var cmd = GetCommandAndArgs("setPos", out var args);
            var loc = location.LocalToWorld();
            args.Add(loc.X); args.Add(loc.Y); args.Add(loc.Z);
            Protocol.Send(cmd, args.Select(x => x.ToString()).ToArray());
        }

        /// <summary>
        /// Get the entities heading
        /// </summary>
        public Heading GetHeading()
        {
            var cmd = GetCommandAndArgs("getRotation", out var args);
            var rotation = double.Parse(Protocol.SendReceive(cmd, args.Select(x => x.ToString()).ToArray()));
            // Convert to heading
            if (rotation < 0)
                rotation = rotation + 360;
            if ((rotation >= 45) && (rotation < 135))
                return Heading.West;
            if ((rotation >= 135) && (rotation < 225))
                return Heading.North;
            if ((rotation >= 225) && (rotation < 315))
                return Heading.East;
            return Heading.South;
        }

        /// <summary>
        /// Get a transform to convert player local co-ordinates to
        /// world co-ordinates.
        /// </summary>
        public Transform GetTransform()
        {
            var heading = GetHeading();
            var transform = PlayerRotation[heading];
            var where = GetLocation().Move(heading, 1);
            return transform.Apply(Transform.Translate(where.X, where.Y, where.Z));
        }

        /// <summary>
        /// Post a message to the chat
        /// </summary>
        public void PostMessage(string message)
        {
            API.PostMessage($"@{Name} {message}");
        }
        #endregion

        #region Implementation of building API
        /// <summary>
        /// Get the block at the given location
        /// </summary>
        public int GetBlock(Location location)
        {
            return API.GetBlock(location);
        }

        /// <summary>
        /// Set the block at the given location
        /// </summary>
        public void SetBlock(Location location, int block)
        {
            API.SetBlock(location, block);
        }

        /// <summary>
        /// Spawn a new entity
        /// </summary>
        public int SpawnEntity(Location location, int entityId)
        {
            return API.SpawnEntity(location, entityId);
        }


        /// <summary>
        /// Get the height of the world at the given x, y location
        /// </summary>
        public Location GetHeight(Location location)
        {
            return API.GetHeight(location);
        }
        #endregion
    }
}
