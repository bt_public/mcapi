﻿namespace FlipSky.MineCraft.API
{
    internal class HitEvent : IHitEvent
    {
        /// <summary>
        /// Type of hit event
        /// </summary>
        public int Type { get; private set; }

        /// <summary>
        /// Location of hit event
        /// </summary>
        public Location Location { get; private set; }

        /// <summary>
        /// Face???
        /// </summary>
        public int Face { get; private set; }

        /// <summary>
        /// Entity that performed the hit
        /// </summary>
        public int EntityId { get; private set; }

        /// <summary>
        /// Decode from arguments
        /// </summary>
        /// <param name="args"></param>
        public HitEvent(string[] args)
        {
            Type = int.Parse(args[0]);
            Location = new Location(
                int.Parse(args[1]),
                int.Parse(args[2]),
                int.Parse(args[3])).WorldToLocal();
            Face = int.Parse(args[4]);
            EntityId = int.Parse(args[5]);
        }
    }
}
