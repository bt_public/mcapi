﻿using System;
using System.Net.Sockets;
using System.Text;
using FlipSky.Logging;
using FlipSky.Depends;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// The low level server connection
    /// </summary>
    [DefaultImplementation(typeof(IServer))]
    public class Server : IServer, IProtocol
    {
        //-- Constants
        private const int BufferSize = 2048;
        private const int MaxBufferSize = 1024 * 1024;
        private const int MaxLogSize = 64;

        //-- State
        private readonly object AtomicOperation = new object();
        private Socket ClientSocket;
        private byte[] ReadBuffer = new byte[BufferSize];

        //--- Injections
        private readonly ILogger Log;

        /// <summary>
        /// Constructor with injections
        /// </summary>
        public Server(ILogger log)
        {
            Log = log;
        }

        #region Helpers
        /// <summary>
        /// Test for an active connection. If the connection was active but is no closed
        /// throw a ConnectionDroppedException
        /// </summary>
        private bool TestForActiveConnection()
        {
            if (ClientSocket == null)
                return false;
            // Do we still have a connection?
            if (!ClientSocket.Connected)
            {
                ClientSocket = null;
                throw new ConnectionDroppedException();
            }
            return true;
        }

        /// <summary>
        /// Test the connection before performing a network operation. Will
        /// throw an exception if a connection is not available.
        /// </summary>
        private void FailOnInactiveConnection()
        {
            if (!TestForActiveConnection())
                throw new MineCraftApiException("No active connection to server.");
        }

        /// <summary>
        /// Create a (larger) copy of the read buffer and return it. This is for operations
        /// that return larger response packets (getBlocks for example).
        /// </summary>
        private byte[] IncreaseReadBufferSize(byte[] buffer)
        {
            if (buffer.Length >= MaxBufferSize)
                throw new MineCraftApiException($"Read buffer has exceed maximum size of {MaxBufferSize} bytes.");
            byte[] increased = new byte[buffer.Length * 2];
            Buffer.BlockCopy(buffer, 0, increased, 0, buffer.Length);
            return increased;
        }

        /// <summary>
        /// Read a response line from the server
        /// </summary>
        private string ReadResponseLine()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Public API
        /// <summary>
        /// Establish the connection
        /// </summary>
        public IMineCraft Connect(string hostname, int port)
        {
            lock (AtomicOperation)
            {
                if (TestForActiveConnection())
                    throw new MineCraftApiException("Connection already established.");
                // Create the socket and connect
                ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                ClientSocket.Connect(hostname, port);
            }
            return new MineCraftApi(this, Log);
        }
        #endregion

        #region Protocol API
        /// <summary>
        /// Drop the connection
        /// </summary>
        public void Disconnect()
        {
            lock (AtomicOperation)
            {
                try
                {
                    if (TestForActiveConnection())
                    {
                        ClientSocket.Close();
                    }
                }
                catch (ConnectionDroppedException)
                {
                    Log.Warn("Closing connection but server has already closed it.");
                }
                catch (Exception ex)
                {
                    Log.Warn(ex, "Error while closing connection.");
                }
                finally
                {
                    ClientSocket = null;
                }
            }
        }

        /// <summary>
        /// Send a function (with arguments)
        /// </summary>
        public void Send(string function, params string[] args)
        {
            // First, create the packet to send and convert it to CP437 code page
            var command = $"{function}(" + string.Join(",", args) + ")\n";
            var packet = Encoding.ASCII.GetBytes(command);
            // Send it to the server
            lock (AtomicOperation)
            {
                FailOnInactiveConnection();
                // Drain any pending incoming data
                while (ClientSocket.Available > 0)
                    ClientSocket.Receive(ReadBuffer, Math.Min(ClientSocket.Available, ReadBuffer.Length), SocketFlags.None);
                // Send the request
                ClientSocket.Send(packet);
                // Log it
                if (command.Length > MaxLogSize)
                    Log.Debug("SEND:{0}", command.Substring(0, MaxLogSize - 4) + " ...");
                else
                    Log.Debug("SEND:{0}", command.TrimEnd());
            }
        }

        /// <summary>
        /// Receive data from the server as a string
        /// </summary>
        internal string Receive()
        {
            lock (AtomicOperation)
            {
                FailOnInactiveConnection();
                // Read the response until we find a newline char
                int count = 0;
                var buffer = ReadBuffer;
                ClientSocket.Receive(buffer, count, 1, SocketFlags.None);
                while (buffer[count] != 0x0A)
                {
                    count++;
                    if (count >= buffer.Length)
                        buffer = IncreaseReadBufferSize(buffer);
                    ClientSocket.Receive(buffer, count, 1, SocketFlags.None);
                }
                // Convert it to a string (without newline) and return it
                var result = Encoding.ASCII.GetString(buffer, 0, count).TrimEnd();
                // Log it
                if (result.Length > MaxLogSize)
                    Log.Debug("RECV:{0}", result.Substring(0, MaxLogSize - 4) + " ...");
                else
                    Log.Debug("RECV:{0}", result);
                // All done
                return result;
            }
        }

        /// <summary>
        /// Send a function and then receive the response
        /// </summary>
        public string SendReceive(string function, params string[] args)
        {
            Send(function, args);
            return Receive();
        }
        #endregion
    }
}
