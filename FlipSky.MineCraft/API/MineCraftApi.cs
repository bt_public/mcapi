﻿using System;
using System.Linq;
using System.Collections.Generic;
using FlipSky.Logging;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// Implementation of the basic MineCraft server API
    /// </summary>
    internal class MineCraftApi : IMineCraft
    {
        //--- Injections
        internal readonly IProtocol Protocol;
        private readonly ILogger Log;

        /// <summary>
        /// Constructor with injections
        /// </summary>
        public MineCraftApi(IProtocol protocol, ILogger log)
        {
            Protocol = protocol;
            Log = log;
            // TODO: Set child API instances
            Player = new EntityApi(this, null, Log);
            Events = new EventsApi(Protocol, Log);
        }

        #region Child API Access
        /// <summary>
        /// Access to the player
        /// </summary>
        public IEntityApi Player { get; private set; }

        /// <summary>
        /// Access to events API
        /// </summary>
        public IEventsApi Events { get; private set; }
        #endregion

        #region World Level API
        /// <summary>
        /// Get the entity id's for all players
        /// </summary>
        public IEnumerable<int> GetPlayerEntityIds()
        {
            var answer = Protocol.SendReceive("world.getPlayerIds");
            return answer.Split('|').Select(x => int.Parse(x));
        }

        /// <summary>
        /// Get the entity ID of the named player
        /// </summary>
        public bool GetPlayerEntityId(string name, out int entityId)
        {
            var answer = Protocol.SendReceive("world.getPlayerId", name);
            throw new NotImplementedException();
        }

        /// <summary
        /// Post a message to the chat system
        /// </summary>
        public void PostMessage(string message)
        {
            Protocol.Send("chat.post", message);
        }

        /// <summary>
        /// Get the API to work with a specific entity
        /// </summary>
        public IEntityApi GetEntity(int entityId)
        {
            return new EntityApi(this, entityId, Log);
        }

        #endregion

        #region Building API
        /// <summary>
        /// Get the block at the given location
        /// </summary>
        public int GetBlock(Location location)
        {
            var answer = Protocol.SendReceive("world.getBlock", location.LocalToWorld().Encode());
            return int.Parse(answer);
        }

        /// <summary>
        /// Set the block at the given location
        /// </summary>
        public void SetBlock(Location location, int block)
        {
            Protocol.Send("world.setBlock", location.LocalToWorld().Encode(), block.ToString());
        }

        /// <summary>
        /// Spawn a new entity
        /// </summary>
        public int SpawnEntity(Location location, int entityId)
        {
            var answer = Protocol.SendReceive("world.spawnEntity", location.LocalToWorld().Encode(), entityId.ToString());
            return int.Parse(answer);
        }

        /// <summary>
        /// Get the height of the world at the given x, y location
        /// </summary>
        public Location GetHeight(Location location)
        {
            var world = location.LocalToWorld();
            var answer = Protocol.SendReceive("world.getHeight", world.X.ToString(), world.Z.ToString());
            var z = int.Parse(answer);
            return new Location(location.X, location.Y, z);
        }
        #endregion

        #region Connection Control
        /// <summary>
        /// Determine if we have a connection or not
        /// </summary>
        public bool Connected { get; }

        /// <summary>
        /// Disconnect from the server
        /// </summary>
        public void Disconnect()
        {
            Protocol.Disconnect();
        }
        #endregion

    }
}
