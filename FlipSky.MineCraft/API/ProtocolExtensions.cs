﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// Helpers to encode and decode values for the MCAPI protocol
    /// </summary>
    internal static class ProtocolExtensions
    {
        /// <summary>
        /// Encode a single location
        /// </summary>
        public static string Encode(this Location location)
        {
            return $"{location.X},{location.Y},{location.Z}";
        }
    }
}
