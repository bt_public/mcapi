﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// Basic protocol operations
    /// </summary>
    internal interface IProtocol
    {
        /// <summary>
        /// Drop the connection
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Send a command to the server
        /// </summary>
        void Send(string function, params string[] args);

        /// <summary>
        /// Send a command and get the response
        /// </summary>
        string SendReceive(string function, params string[] args);
    }
}
