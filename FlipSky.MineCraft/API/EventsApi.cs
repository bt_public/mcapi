﻿using System.Linq;
using System.Collections.Generic;
using FlipSky.Logging;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// Implementation of IEventApi
    /// </summary>
    internal class EventsApi : IEventsApi
    {
        //--- Injections
        private readonly ILogger Log;
        private readonly IProtocol Protocol;

        /// <summary>
        /// Constructor with injections
        /// </summary>
        public EventsApi(IProtocol protocol, ILogger log)
        {
            Protocol = protocol;
            Log = log;
        }

        #region Events API
        /// <summary>
        /// Get all chat events that have occurred since the last call
        /// </summary>
        public IEnumerable<IChatEvent> GetChatEvents()
        {
            var answer = Protocol.SendReceive("events.chat.posts");
            return answer.Split('|')
                .Where(ev => ev.Length > 0)
                .Select(ev => new ChatEvent(ev.Split(',')));
        }

        /// <summary>
        /// Get all hit events that have occurred since the last call
        /// </summary>
        public IEnumerable<IHitEvent> GetHitEvents()
        {
            var answer = Protocol.SendReceive("events.chat.posts");
            return answer.Split('|').Select(ev => new HitEvent(ev.Split(',')));
        }

        /// <summary>
        /// Flush all events since the last check
        /// </summary>
        public void FlushEvents()
        {
            Protocol.Send("events.clear");
        }
        #endregion
    }
}
