﻿using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft.API
{
    /// <summary>
    /// Thrown when the connection as dropped, use it to reconnect
    /// </summary>
    public class ConnectionDroppedException : MineCraftException
    {
        public ConnectionDroppedException() { }

        public ConnectionDroppedException(string message) : base(message) { }

        public ConnectionDroppedException(string message, Exception innerException) : base(message, innerException) { }

        protected ConnectionDroppedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
