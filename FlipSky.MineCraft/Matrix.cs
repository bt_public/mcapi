﻿using System;
using System.Linq;
using FlipSky.Ensures;

namespace FlipSky.MineCraft
{
    public class Matrix : IEquatable<Matrix>
    {
        /// <summary>
        /// Number of rows in the matrix
        /// </summary>
        public int Rows { get; private set; }

        /// <summary>
        /// Number of columns in the matrix
        /// </summary>
        public int Cols { get; private set; }

        /// <summary>
        /// The actual values
        /// </summary>
        protected readonly double[] Values;

        /// <summary>
        /// Constructor with optional additional data
        /// </summary>
        public Matrix(int rows, int cols, params double[] data)
        {
            Ensure.IsTrue<ArgumentException>(rows > 0);
            Ensure.IsTrue<ArgumentException>(cols > 0);
            Ensure.IsTrue<ArgumentException>((data.Length == 0) || (data.Length == rows * cols));
            // Set initial state
            Rows = rows;
            Cols = cols;
            Values = data.Length > 0 ? data : new double[rows * cols];
        }

        /// <summary>
        /// Get the index of a specified element
        /// </summary>
        private int IndexOf(int r, int c)
        {
            return r * Cols + c;
        }

        /// <summary>
        /// Get the element at the specific position
        /// </summary>
        public double GetElement(int row, int col)
        {
            Ensure.IsTrue<ArgumentException>((row >= 0) && (row < Rows));
            Ensure.IsTrue<ArgumentException>((col >= 0) && (col < Cols));
            return Values[IndexOf(row, col)];
        }

        /// <summary>
        /// Multiply this matrix by a scalar value
        /// </summary>
        public Matrix Multiply(double value)
        {
            return new Matrix(Rows, Cols, Values.Select(v => v * value).ToArray());
        }

        /// <summary>
        /// Multiple this matrix by another matrix
        /// </summary>
        public Matrix Multiply(Matrix rhs)
        {
            Ensure.IsNotNull<ArgumentException>(rhs);
            Ensure.IsTrue<ArgumentException>(Cols == rhs.Rows);
            // This is not at all optimised
            var result = new Matrix(Rows, rhs.Cols);
            for (int row = 0; row < Rows; row++)
                for (int col = 0; col < rhs.Cols; col++)
                {
                    var element = 0.0;
                    for (int i = 0; i < Cols; i++)
                        element = element + Values[IndexOf(row, i)] * rhs.Values[rhs.IndexOf(i, col)];
                    result.Values[result.IndexOf(row, col)] = element;
                }
            // Done
            return result;
        }

        /// <summary>
        /// Do the multiplication and return the raw values
        /// </summary>
        public static double[] Multiply(Matrix lhs, Matrix rhs)
        {
            var result = lhs.Multiply(rhs);
            return result.Values;
        }

        /// <summary>
        /// Test for equality with another matrix
        /// </summary>
        public bool Equals(Matrix other)
        {
            // Short circuit the full comparison if we can
            if (this == other)
                return true;
            if ((other == null) || (other.Rows != Rows) || (other.Cols != Cols))
                return false;
            // Test elements
            for (int i = 0; i < Values.Length; i++)
                if (Values[i] != other.Values[i])
                    return false;
            // Same matrix
            return true;
        }

        /// <summary>
        /// Test for equality with another object
        /// </summary>
        public override bool Equals(object obj)
        {
            return Equals(obj as Matrix);
        }

        /// <summary>
        /// Generate a hash code for the matrix
        /// </summary>
        public override int GetHashCode()
        {
            return Rows * Cols;
        }
    }
}
