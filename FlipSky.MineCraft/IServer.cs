﻿namespace FlipSky.MineCraft
{
    /// <summary>
    /// Defines the interface for connecting to a MineCraft server
    /// </summary>
    public interface IServer
    {
        /// <summary>
        /// Establish a connection to the server
        /// </summary>
        IMineCraft Connect(string hostname, int port);
    }
}
