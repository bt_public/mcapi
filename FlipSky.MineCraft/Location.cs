using System;

namespace FlipSky.MineCraft 
{
    /// <summary>
    /// Represents a location in the world
    /// </summary>
    public class Location : Matrix, IEquatable<Location>
    {
        /// <summary>
        /// Hash code for this instance
        /// </summary>
        private readonly int HashCode;

        /// <summary>
        /// The Zero location
        /// </summary>
        public static readonly Location Zero = new Location(0, 0, 0);

        /// <summary>
        /// X location
        /// </summary>
        public int X => (int)GetElement(0, 0);

        /// <summary>
        /// Y location
        /// </summary>
        public int Y => (int)GetElement(0, 1);

        /// <summary>
        /// Z location
        /// </summary>
        public int Z => (int)GetElement(0, 2);

        /// <summary>
        /// Constructor with values
        /// </summary>
        public Location(int x, int y, int z) : base(1, 4, x, y, z, 1.0)
        {
            // Use the hash of the generated string
            HashCode = ToString().GetHashCode();
        }

        /// <summary>
        /// Return a string representation of the location
        /// </summary>
        public override string ToString()
        {
            return $"({X}, {Y}, {Z})";
        }

        /// <summary>
        /// Generate a hashcode for this object
        /// </summary>
        public override int GetHashCode() 
        {
            return HashCode;
        }

        /// <summary>
        /// Test equality with any other object
        /// </summary>
        public override bool Equals(object obj) 
        {
            return Equals(obj as Location);
        }

        /// <summary>
        /// Test equality to other Location instances
        /// </summary>
        public bool Equals(Location obj) 
        {
            return obj != null && obj.X == X && obj.Y == Y && obj.Z == Z;
        }

    }
}