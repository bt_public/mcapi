﻿using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft
{
    /// <summary>
    /// Base exception for all MineCraft library related errors.
    /// </summary>
    public class MineCraftException : Exception
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        public MineCraftException() { }

        /// <summary>
        /// Constructor with a message
        /// </summary>
        public MineCraftException(string message) : base(message) { }

        /// <summary>
        /// Constructor with inner exception
        /// </summary>
        public MineCraftException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Constructor with serialization information
        /// </summary>
        protected MineCraftException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
