﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace FlipSky.MineCraft.Building
{
    /// <summary>
    /// Location generators for various shapes
    /// </summary>
    public static class Generators
    {
        /// <summary>
        /// Iterator over all blocks in a region
        /// </summary>
        private class RegionIterator : IEnumerable<Location>
        {
            //--- Range
            private readonly Location BottomLeft;
            private readonly Location TopRight;

            /// <summary>
            /// Constructor
            /// </summary>
            public RegionIterator(Location c1, Location c2)
            {
                Transforms.BoundingBox(c1, c2, out BottomLeft, out TopRight);
            }

            /// <summary>
            /// Iterate over all points from the lowest to the highest
            /// </summary>
            public IEnumerator<Location> GetEnumerator()
            {
                for (int z = BottomLeft.Z; z < TopRight.Z; z++)
                    for (int x = BottomLeft.X; x < TopRight.X; x++)
                        for (int y = BottomLeft.Y; y < TopRight.Y; y++)
                            yield return new Location(x, y, z);
            }

            /// <summary>
            /// Untyped enumerator
            /// </summary>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        /// <summary>
        /// Factory for the Enumerator
        /// </summary>
        public static IEnumerable<Location> Region(Location c1, Location c2)
        {
            return new RegionIterator(c1, c2);
        }

        /// <summary>
        /// Enumerate over all the line points for an arc (1/4 circle). The
        /// returned locations are all deltas in x,y only.
        /// </summary>
        public static IEnumerable<Location> Arc(int radius)
        {
            var results = new List<Location>();
            // Use the midpoint algorithm
            int x = radius;
            int y = 0;
            int p = 1 - radius;
            results.Add(new Location(x, y, 0));
            results.Add(new Location(y, x, 0));
            while (x > y)
            {
                y = y + 1;
                if (p <= 0)
                    p = p + 2 * y + 1;
                else
                {
                    x = x - 1;
                    p = p + 2 * y - 2 * x + 1;
                }
                if (x < y)
                    break;
                results.Add(new Location(x, y, 0));
                results.Add(new Location(y, x, 0));
            }
            return results.OrderBy(l => l.X).ThenByDescending(l => l.Y);
        }

        /// <summary>
        /// Enumerate over all points for a line from 0, 0, 0 to x, y 0.
        /// The points can be transformed to generate a line in any plane.
        /// </summary>
        public static IEnumerable<Location> Line(int x, int y)
        {
            throw new NotImplementedException();
        }
    }
}
