﻿using System;
using System.Collections.Generic;

namespace FlipSky.MineCraft.Building
{
    /// <summary>
    /// A list of block changes to make
    /// </summary>
    public class BlockChangeList : List<BlockChange>
    {
        /// <summary>
        /// Save the change list to a file
        /// </summary>
        public void Save(string filename)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Load the changelist from a file
        /// </summary>
        public static BlockChangeList Load(string filename)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Apply the change list to a world from an entity viewpoint.
        /// 
        /// Build co-ordinates will be adjusted to world co-ordinates before
        /// application.
        /// </summary>
        public void ApplyTo(IEntityApi entity)
        {
            var transform = entity.GetTransform();
            foreach (var change in this)
                entity.SetBlock(
                    transform.Apply(change.Location),
                    change.Block
                    );
        }

        /// <summary>
        /// Apply the change list to a build buffer.
        /// </summary>
        public void ApplyTo(IBuildBuffer buffer)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Read blocks from a region in the world (in world co-ordinates)
        /// and generate a change list to represent it. The change list is
        /// in build co-ordinates with the players position and heading
        /// indicating the origin and direction of x, y axis.
        /// </summary>
        public void FromWorld(IEntityApi entity, Location c1, Location c2)
        {
            throw new NotImplementedException();
        }
    }
}
