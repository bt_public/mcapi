﻿using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft.Building
{
    /// <summary>
    /// Exception for building related errors
    /// </summary>
    public class MineCraftBuildException : MineCraftException
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        public MineCraftBuildException() { }

        /// <summary>
        /// Constructor with a message
        /// </summary>
        public MineCraftBuildException(string message) : base(message) { }

        /// <summary>
        /// Constructor with inner exception
        /// </summary>
        public MineCraftBuildException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// Constructor with serialization information
        /// </summary>
        protected MineCraftBuildException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
