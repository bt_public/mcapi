﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipSky.MineCraft.Building
{
    public static class Transforms
    {
        /// <summary>
        /// Convert a set of co-ordinates into a bounding box specified by
        /// bottom left and top right.
        /// </summary>
        public static void BoundingBox(Location c1, Location c2, out Location bottomLeft, out Location topRight)
        {
            bottomLeft = new Location(
                Math.Min(c1.X, c2.X),
                Math.Min(c1.Y, c2.Y),
                Math.Min(c1.Z, c2.Z)
                );
            topRight = new Location(
                Math.Max(c1.X, c2.X),
                Math.Max(c1.Y, c2.Y),
                Math.Max(c1.Z, c2.Z)
                );
        }
    }
}
