﻿namespace FlipSky.MineCraft.Building
{
    /// <summary>
    /// Represents a single change to make in the world location
    /// </summary>
    public class BlockChange
    {
        /// <summary>
        /// The location of the change
        /// </summary>
        public Location Location { get; private set; }

        /// <summary>
        /// The block to set at the position
        /// </summary>
        public int Block { get; private set; }

        /// <summary>
        /// Constructor with initialisers
        /// </summary>
        public BlockChange(Location location, int block)
        {
            Location = location;
            Block = block;
        }
    }
}
