﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlipSky.MineCraft.Building
{
    /// <summary>
    /// Extension methods to help build various items
    /// </summary>
    public static class BuildExtensions
    {
        /// <summary>
        /// Fill a region of the buffer with the given block
        /// </summary>
        public static void Fill(this IBuildBuffer buffer, Location c1, Location c2, int block)
        {
            foreach (var location in Generators.Region(c1, c2))
                buffer.SetBlock(location, block);
        }

        /// <summary>
        /// Draw an arc segment with the given block
        /// </summary>
        public static void Arc(this IBuildBuffer buffer, Location c1, int radius, int segment, int block, bool vertical = false)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Draw a complete (optionally filled) circle with the given block
        /// </summary>
        public static void Circle(this IBuildBuffer buffer, Location c1, int radius, int block, bool fill = false, bool vertical = false)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Draw a complete sphere (optionally filled) at the given location
        /// </summary>
        public static void Sphere(this IBuildBuffer buffer, Location c1, int radius, int block, bool fill)
        {
            throw new NotImplementedException();
        }
    }
}
