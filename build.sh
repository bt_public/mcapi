#!/bin/bash
#----------------------------------------------------------------------------
# Build script.
#
# Use a single script so we can run everything in a single container
#----------------------------------------------------------------------------

# Versioning
VERSION_PREFIX=1.0
VERSION_SUFFIX=-pre

# Target
TARGET_CONFIG=Debug

#--- Set environment based on parameter
if [ "X$1" == "Xdeploy" ]; then
  TARGET_CONFIG=Release
  if [ "X${CI_COMMIT_REF_NAME}" == "Xrelease" ]; then
    VERSION_SUFFIX=
  fi
fi

#--- Build the full version number
VERSION=${VERSION_PREFIX}.${CI_PIPELINE_IID}${VERSION_SUFFIX}
echo "*** Buiding version ${VERSION}/${TARGET_CONFIG}"


# Build and test
dotnet build --configuration ${TARGET_CONFIG} || exit 1
dotnet test --configuration ${TARGET_CONFIG} || exit 1

echo "*** Deploying for target ${TARGET_CONFIG}"

if [ "X${TARGET_CONFIG}" == "XRelease" ]; then
  if [ "X${NUGET_KEY}" == "X" ]; then
    echo "ERROR: No NuGet deploy key provided"
    exit 1
  fi

  # Build NuGet Packages
  PKGDIR=$(pwd)/nupkg
  mkdir -p ${PKGDIR}
  dotnet pack FlipSky.MineCraft --configuration Release --output ${PKGDIR} -p:Version=${VERSION} || exit 1
  dotnet pack FlipSky.MineCraft.Scripting --configuration Release --output ${PKGDIR} -p:Version=${VERSION} || exit 1
  echo "Packages are:"
  ls ${PKGDIR}/*.nupkg
  echo "Publishing ..."
  for pkg in `ls ${PKGDIR}/*.nupkg`; do
    dotnet nuget push ${pkg} -k ${NUGET_KEY} -s https://api.nuget.org/v3/index.json  || exit 1
  done

  # Build containers
  dotnet publish McChat --self-contained --output $(pwd)/container/app --runtime linux-musl-x64 || exit 1
  docker build -t mcchat:${VERSION} container || exit 1
  docker tag mcchat:${VERSION} flipsky/mcchat:${VERSION} || exit 1

  if [ "X${DOCKER_USER}" == "X" -o "X${DOCKER_PASS}" == "X" ]; then
    echo "ERROR: No Docker deployment credentials provided"
    exit 1
  fi

  docker login -u ${DOCKER_USER} -p ${DOCKER_PASS} || exit 1
  docker push flipsky/mcchat:${VERSION} || exit 1
else
  echo "Target configuration is not Release, no deployment for you."
fi
