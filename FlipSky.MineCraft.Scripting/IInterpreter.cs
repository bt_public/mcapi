using FlipSky.MineCraft.Building;

namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Simple interpreter interface
    /// </summary>
    public interface IInterpreter
    {
        /// <summary>
        /// Execute the code block into the given buffer
        /// </summary>
        void Execute(IBuildBuffer buffer);
    }
}