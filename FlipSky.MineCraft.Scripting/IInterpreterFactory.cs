namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Factory class for interpreters
    /// </summary>
    internal interface IInterpreterFactory
    {
        /// <summary>
        /// Create a new interpreter to run the given sequence
        /// </summary>
        IInterpreter Create(Sequence sequence);
    }
}