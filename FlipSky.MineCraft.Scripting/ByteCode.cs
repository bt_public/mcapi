namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Bytecodes supported by the VM
    /// </summary>
    internal enum ByteCode
    {
        Move,      // Move relative to current location
        Goto,      // Go to absolute co-ordinate
        Use,       // Select a block to use
        Exec,      // Execute a procedure
        Loop,      // Execute a loop
        Set,       // Set a variable value
        Fill,      // Fill a region with the current block
        ArcPoints, // Generate points for an arc
        LinePoints // Generate points for a line
    }
}
