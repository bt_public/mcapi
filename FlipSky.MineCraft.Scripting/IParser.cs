using System.Collections.Generic;

namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Interface to the parser
    /// </summary>
    public interface IParser
    {
        /// <summary>
        /// Parse the given source and return an interpreter for the resulting
        /// code.
        /// </summary>
        IInterpreter Parse(string source);
    }
}