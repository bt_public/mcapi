﻿using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft.Scripting.Parser
{
    /// <summary>
    /// Exception for preprocessor errors
    /// </summary>
    public class ParserException : ScriptingException
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        public ParserException() { }

        /// <summary>
        /// Constructor with a message
        /// </summary>
        public ParserException(string message) : base(message) { }

        /// <summary>
        /// Constructor with inner exception
        /// </summary>
        public ParserException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Constructor with serialization information
        /// </summary>
        protected ParserException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}