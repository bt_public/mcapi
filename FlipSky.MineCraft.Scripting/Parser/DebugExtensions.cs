﻿using System;
using Eto.Parse;

namespace FlipSky.MineCraft.Scripting.Parser
{
    /// <summary>
    /// Some helpers to use while debugging
    /// </summary>
    public static class DebugExtensions
    {
        /// <summary>
        /// Dump the parse tree to the console
        /// </summary>
        public static void Dump(this Match match, string prefix = "")
        {
            foreach (var m in match.Matches)
            {
                Console.WriteLine($"{prefix}{m.Name}");
                Dump(m, prefix + "  ");
            }
        }
    }
}
