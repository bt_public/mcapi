using System;
using System.Linq;
using System.Collections.Generic;
using Eto.Parse;
using Eto.Parse.Grammars;
using FlipSky.Resource;
using FlipSky.Logging;
using FlipSky.Depends;

namespace FlipSky.MineCraft.Scripting.Parser
{
    /// <summary>
    /// Implementation of the parser
    /// </summary>
    [DefaultImplementation(typeof(IParser))]
    internal class ScriptParser : IParser
    {
        /// <summary>
        /// Delegate for processing individual instruction AST nodes
        /// </summary>
        private delegate void InstructionParser(Match tree, IDictionary<string, Sequence> procs, Sequence target);

        //--- Constants
        private const string GrammarFile = "grammar.ebnf";

        //--- Injections
        private readonly ILogger Log;
        private readonly IInterpreterFactory InterpreterFactory;

        //--- AST Node mappings
        private readonly Dictionary<string, InstructionParser> InstructionParsers;

        /// <summary>
        /// Constructor with injections
        /// </summary>
        public ScriptParser(IInterpreterFactory interpreterFactory, ILogger log)
        {
            Log = log;
            InterpreterFactory = interpreterFactory;
            // Set up parser mappings. They must match the rule definitions in the syntax
            InstructionParsers = new Dictionary<string, InstructionParser>()
            {
                { "cmdmove",    ParseThreeArgCommand },
                { "cmdgoto",    ParseThreeArgCommand },
                { "cmduse",     ParseUseCommand },
                { "cmdset",     ParseSetCommand },
                { "cmdfill",    ParseThreeArgCommand },
                { "cmdexec",    ParseExecCommand },
                { "ctlloop",    ParseLoopBlock },
                { "ctlarcpts",  ParsePointsBlock },
                { "ctllinepts", ParsePointsBlock },
            };
        }

        #region Parser Delegates
        /// <summary>
        /// Parse commands that required three expressions as parameters
        /// </summary>
        private void ParseThreeArgCommand(Match tree, IDictionary<string, Sequence> procs, Sequence target)
        {
            var args = tree.Matches.Where(a => a.Name == "expression")
                .Select(a => (object)Expressions.Parse(a))
                .ToArray();
            // Emit the bytecode
            switch (tree.Name)
            {
                case "cmdmove": target.Emit(ByteCode.Move, args); break;
                case "cmdgoto": target.Emit(ByteCode.Goto, args); break;
                case "cmdfill": target.Emit(ByteCode.Fill, args); break; 
                default:
                    throw new ParserException($"Incorrect parse routing for token '{tree.Name}'");
            }
        }

        /// <summary>
        /// Parse the use command
        /// </summary>
        private void ParseUseCommand(Match tree, IDictionary<string, Sequence> procs, Sequence target)
        {
            var arg = tree.Matches[0];
            int blk;
            // Expecting an identifier
            if (arg.Name == "number")
            {
                if (!int.TryParse(arg.StringValue, out blk))
                    throw new ParserException($"Invalid integer token '{arg.StringValue}'");
            }
            else if (arg.Name == "identifier")
            {
                if (!Blocks.TryParse(arg.StringValue, out blk))
                    throw new ParserException($"Undefined block name '{arg.StringValue}'");
            }
            else
                throw new ParserException($"Expecting 'identifier', got '{arg.Name}'");
            // Add it
            target.Emit(ByteCode.Use, blk);
        }

        /// <summary>
        /// Parse the the set command
        /// </summary>
        private void ParseSetCommand(Match tree, IDictionary<string, Sequence> procs, Sequence target)
        {
            var args = new object[2];
            var arg = tree.Matches[0];
            // Expecting an identifier
            if (arg.Name != "identifier")
                throw new ParserException($"Expecting 'identifier', got '{arg.Name}'");
            args[0] = arg.StringValue;
            // Expecting an expression
            arg = tree.Matches[1];
            if (arg.Name != "expression")
                throw new ParserException($"Expecting 'expression', got '{arg.Name}'");
            args[1] = Expressions.Parse(arg);
            // Add the code
            target.Emit(ByteCode.Set, args);
        }

        /// <summary>
        /// Parse the exec command
        /// </summary>
        private void ParseExecCommand(Match tree, IDictionary<string, Sequence> procs, Sequence target)
        {
            var arg = tree.Matches[0];
            // Expecting an identifier
            if (arg.Name != "identifier")
                throw new ParserException($"Expecting 'identifier', got '{arg.Name}'");
            // Must be a previously defined procededure
            if (!procs.TryGetValue(arg.StringValue, out var code))
                throw new ParserException($"Undefined procedure '{arg.StringValue}'");
            // Get the arguments for the proc
            var args = tree.Matches.Where(a => a.Name == "expression")
                .Select(a => (object)Expressions.Parse(a))
                .ToList();
            // Insert the code at the begining and emit the opocode
            args.Insert(0, code);
            target.Emit(ByteCode.Exec, args.ToArray<object>());
        }

        /// <summary>
        /// Parse the loop block
        /// </summary>
        private void ParseLoopBlock(Match tree, IDictionary<string, Sequence> procs, Sequence target)
        {
            var args = new object[2];
            var arg = tree.Matches[0];
            // Expecting an expression
            if (arg.Name != "expression")
                throw new ParserException($"Expecting 'expression', got '{arg.Name}'");
            args[1] = Expressions.Parse(arg);
            // Expecting a codeblock
            arg = tree.Matches[1];
            if (arg.Name != "codeblock")
                throw new ParserException($"Expecting 'codeblock', got '{arg.Name}'");
            args[0] = ParseCodeBlock(arg, procs);
            // Add the code
            target.Emit(ByteCode.Loop, args);
        }

        /// <summary>
        /// Parse the arcpts block
        /// </summary>
        private void ParsePointsBlock(Match tree, IDictionary<string, Sequence> procs, Sequence target)
        {
            // Get the arguments
            var args = tree.Matches.Where(a => a.Name == "expression")
                .Select(a => (object)Expressions.Parse(a))
                .ToList();
            // Get the codeblock (should be last)
            var arg = tree.Matches[tree.Matches.Count - 1];
            if (arg.Name != "codeblock")
                throw new ParserException($"Expecting 'codeblock', got '{arg.Name}'");
            args.Insert(0, ParseCodeBlock(arg, procs));
            // Add the code
            switch (tree.Name)
            {
                case "ctlarcpts": target.Emit(ByteCode.ArcPoints, args.ToArray<object>()); break;
                case "ctllinepts": target.Emit(ByteCode.LinePoints, args.ToArray<object>()); break;
                default:
                    throw new ParserException($"Incorrect parse routing for token '{tree.Name}'");
            }
        }
        #endregion

        #region Parse Helpers
        /// <summary>
        /// Parse a code block
        /// </summary>
        private Sequence ParseCodeBlock(Match tree, IDictionary<string, Sequence> procs)
        {
            var target = new Sequence();
            foreach (var node in tree.Matches.Where(n => n.Name == "instruction"))
            {
                // Find a parser
                var instruction = node.Matches[0];
                if (!InstructionParsers.TryGetValue(instruction.Name, out var parser))
                    throw new ParserException($"Instruction '{instruction.Name}' is not yet supported.");
                parser(instruction, procs, target);
            }
            return target;
        }

        /// <summary>
        /// Parse a procedure and return the new sequence
        /// </summary>
        private Sequence ParseProc(Match tree, IDictionary<string, Sequence> procs)
        {
            var name = tree["identifier"].StringValue;
            if (procs.ContainsKey(name))
                throw new ParserException($"Procedure '{name}' is already defined.");
            // Parse the body and return it
            procs[name] = ParseCodeBlock(tree["codeblock"], procs);
            return procs[name];
        }
        #endregion

        /// <summary>
        /// Parse the source and return an interpreter for the resulting
        /// code.
        /// </summary>
        public IInterpreter Parse(string source)
        {
            // Verify parameters
            if (string.IsNullOrWhiteSpace(source))
                throw new ParserException("No script source was provided.");
            // Construct the parser and create the tree
            try
            {
                var resources = new ResourceFiles(typeof(ScriptParser));
                var parser = new EbnfGrammar(EbnfStyle.Iso14977)
                    .Build(resources.ReadAsString(GrammarFile), "start");
                var result = parser.Match(source);
                if (!result.Success)
                    throw new ParserException(result.ErrorMessage);
                // DEBUG: result.Dump();
                // Process all the procedures
                var procedures = new Dictionary<string, Sequence>();
                foreach (var token in result.Matches.Where(t => t.Name == "procedure"))
                    ParseProc(token, procedures);
                // Parse the main program and create an interpreter for it
                var sequence = ParseCodeBlock(result["program"]["codeblock"], procedures);
                return InterpreterFactory.Create(sequence);
            }
            catch (ParserException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ParserException("Unexpected error while parsing source.", ex);
            }
        }
    }
}