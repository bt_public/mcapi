﻿namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Interface to manage variables
    /// </summary>
    internal interface IContext
    {
        /// <summary>
        /// The current location
        /// </summary>
        Location Location { get; set; }

        /// <summary>
        /// The current block
        /// </summary>
        int Block { get; set; }

        /// <summary>
        /// Set the value of a variable for the current context
        /// </summary>
        void SetVariable(string name, int value);

        /// <summary>
        /// Get the value of a variable for the current context
        /// </summary>
        int GetVariable(string name);

        /// <summary>
        /// Enter a new context frame
        /// </summary>
        void Push();

        /// <summary>
        /// Exit the current frame and restore the previous
        /// </summary>
        void Pop();
    }
}
