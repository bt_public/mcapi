using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Factory class for operations
    /// </summary>
    internal class Operation
    {
        private class ByteCodeInfo
        {
            /// <summary>
            /// Minimum number of arguments
            /// </summary>
            public int MinArgs { get; set; }

            /// <summary>
            /// Maximum number of arguments
            /// </summary>
            public int MaxArgs { get; set; }
        }

        /// <summary>
        /// Verification information
        /// </summary>
        private static Dictionary<ByteCode, ByteCodeInfo> ByteCodeConstraints = new Dictionary<ByteCode, ByteCodeInfo>()
        {
            { ByteCode.Move, new ByteCodeInfo() { MinArgs = 3, MaxArgs = 3 } },
            { ByteCode.Goto,new ByteCodeInfo() { MinArgs = 3, MaxArgs = 3 } },
            { ByteCode.Use,new ByteCodeInfo() { MinArgs = 1, MaxArgs = 1 } },
            { ByteCode.Exec, new ByteCodeInfo() { MinArgs = 1, MaxArgs = 10 } },
            { ByteCode.Loop, new ByteCodeInfo() { MinArgs = 2, MaxArgs = 2 } },
            { ByteCode.Set, new ByteCodeInfo() { MinArgs = 2, MaxArgs = 2 } },
            { ByteCode.Fill, new ByteCodeInfo() { MinArgs = 3, MaxArgs = 3 } },
            { ByteCode.ArcPoints, new ByteCodeInfo() { MinArgs = 2, MaxArgs = 2 } },
            { ByteCode.LinePoints, new ByteCodeInfo() { MinArgs = 3, MaxArgs = 3 } },
        };

        #region Public Interface
        /// <summary>
        /// The bytecode for the operation
        /// </summary>
        public ByteCode ByteCode { get; private set; }

        /// <summary>
        /// Arguments for the bytecode
        /// </summary>
        public object[] Args { get; private set; }

        /// <summary>
        /// Internal constructor
        /// </summary>
        public Operation(ByteCode byteCode, object[] args)
        {
            // Verify
            if (!ByteCodeConstraints.TryGetValue(byteCode, out var constraint))
                throw new Interpreter.InterpreterException($"Unsupported bytecode '{byteCode}'");
            if ((args == null) || (args.Length < constraint.MinArgs) || (args.Length > constraint.MaxArgs))
                throw new Interpreter.InterpreterException($"Invalid number of arguments for bytecode {byteCode}");
            // Save state
            ByteCode = byteCode;
            Args = args;
        }

        /// <summary>
        /// Generate a string representation with a prefix
        /// </summary>
        public string ToString(string prefix)
        {
            var result = new StringBuilder();
            result.Append($"{prefix}{ByteCode}");
            switch (ByteCode)
            {
                case ByteCode.Exec:
                case ByteCode.Loop:
                case ByteCode.ArcPoints:
                case ByteCode.LinePoints:
                    if (Args.Length > 1)
                    {
                        result.Append(" ");
                        result.Append(string.Join(", ", Args.Skip(1).Select(a => a.ToString())));
                    }
                    result.AppendLine();
                    result.Append((Args[0] as Sequence).ToString(prefix + "  "));
                    break;
                default:
                    if (Args.Length > 0)
                    {
                        result.Append(" ");
                        result.Append(string.Join(", ", Args.Select(a => a.ToString())));
                    }
                    break;
            }
            return result.ToString();
        }

        /// <summary>
        /// Generate a string representation
        /// </summary>
        public override string ToString()
        {
            return ToString(string.Empty);
        }
        #endregion
    }
}