using System;
using System.Collections.Generic;
using Eto.Parse;
using FlipSky.Ensures;

namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Reduce and evaluate expressions
    /// </summary>
    internal static class Expressions
    {
        /// <summary>
        /// Supported operators
        /// </summary>
        public enum Operator
        {
            Multiply,
            Divide,
            Modulus,
            Addition,
            Subtraction
        }

        /// <summary>
        /// The node types
        /// </summary>
        public enum Node
        {
            Operator,
            Constant,
            Variable
        }

        /// <summary>
        /// Base class for expression elements
        /// </summary>
        public abstract class Expression
        {
            /// <summary>
            /// The type of the node
            /// </summary>
            public Node Node { get; private set; }

            /// <summary>
            /// Reduce the expression (attempt to simplify)
            /// </summary>
            public abstract Expression Reduce();

            /// <summary>
            /// Evaluation the expression using the given variables
            /// </summary>
            public abstract int Evaluate(IContext variables);

            /// <summary>
            /// Constructor
            /// </summary>
            protected Expression(Node node)
            {
                Node = node;
            }
        }

        /// <summary>
        /// Represents a variable node
        /// </summary>
        private class VariableNode : Expression
        {
            private string Variable;

            /// <summary>
            /// Reduce the expression (attempt to simplify)
            /// </summary>
            public override Expression Reduce()
            {
                return this;
            }

            /// <summary>
            /// Evaluation the expression using the given variables
            /// </summary>
            public override int Evaluate(IContext variables)
            {
                return variables.GetVariable(Variable);
            }

            public override string ToString()
            {
                return Variable;
            } 

            /// <summary>
            /// Constructor
            /// </summary>
            public VariableNode(string variable) : base(Node.Variable)
            {
                Variable = variable;
            }
        }

        /// <summary>
        /// Represents a constant node
        /// </summary>
        private class ConstantNode : Expression
        {
            private int Constant;

            /// <summary>
            /// Reduce the expression (attempt to simplify)
            /// </summary>
            public override Expression Reduce()
            {
                return this;
            }

            /// <summary>
            /// Evaluation the expression using the given variables
            /// </summary>
            public override int Evaluate(IContext variables)
            {
                return Constant;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            public ConstantNode(int constant) : base(Node.Constant)
            {
                Constant = constant;
            }

            public override string ToString()
            {
                return Constant.ToString();
            } 
        }

        /// <summary>
        /// Represents an operator node
        /// </summary>
        private class OperatorNode : Expression
        {
            private Expression LeftHandSide;
            private Expression RightHandSide;
            private Operator Operator;

            /// <summary>
            /// Reduce the expression (attempt to simplify)
            /// </summary>
            public override Expression Reduce()
            {
                LeftHandSide = LeftHandSide.Reduce();
                RightHandSide = RightHandSide.Reduce();
                if ((LeftHandSide.Node == Node.Constant) && (RightHandSide.Node == Node.Constant))
                {
                    // Evaluate the expression and create a new constant
                    return new ConstantNode(Evaluate(null));
                }
                return this;
            }

            /// <summary>
            /// Evaluation the expression using the given variables
            /// </summary>
            public override int Evaluate(IContext variables)
            {
                switch (Operator)
                {
                    case Operator.Multiply:
                        return LeftHandSide.Evaluate(variables) * RightHandSide.Evaluate(variables);
                    case Operator.Divide:
                        return LeftHandSide.Evaluate(variables) / RightHandSide.Evaluate(variables);
                    case Operator.Modulus:
                        return LeftHandSide.Evaluate(variables) % RightHandSide.Evaluate(variables);
                    case Operator.Addition:
                        return LeftHandSide.Evaluate(variables) + RightHandSide.Evaluate(variables);
                    case Operator.Subtraction:
                        return LeftHandSide.Evaluate(variables) - RightHandSide.Evaluate(variables);
                    default:
                        throw new Interpreter.InterpreterException("Unsupported arithmetic operation.");
                }
            }

            public override string ToString()
            {
                return $"({LeftHandSide} {Operator.ToSymbol()} {RightHandSide})";
            } 

            /// <summary>
            /// Constructor
            /// </summary>
            public OperatorNode(Expression lhs, Operator op, Expression rhs) : base(Node.Variable)
            {
                LeftHandSide = lhs;
                Operator = op;
                RightHandSide = rhs;
            }
        }

        private static Operator ToOperator(this string op)
        {
            switch (op)
            {
                case "+": return Operator.Addition;
                case "-": return Operator.Subtraction;
                case "*": return Operator.Multiply;
                case "/": return Operator.Divide;
                case "%": return Operator.Modulus;
                default: throw new ScriptingException($"Unrecognised operator '{op}'");
            }
        }

        /// <summary>
        /// Convert an operator enum to an operator symbol
        /// </summary>
        private static string ToSymbol(this Operator op)
        {
            switch (op)
            {
                case Operator.Addition: return "+";
                case Operator.Subtraction: return "-";
                case Operator.Multiply: return "*";
                case Operator.Divide: return "/";
                case Operator.Modulus: return "%";
                default:
                    throw new ArgumentException();
            }
        }

        private static Expression ParseExpression(Match match)
        {
            Ensure.IsTrue<ArgumentException>(match.Name == "expression");
            var lhs = ParseTerm(match.Matches[0]);
            for (int i = 1; i < match.Matches.Count;)
            {
                var rhs = ParseTerm(match.Matches[i + 1]);
                lhs = new OperatorNode(lhs, match.Matches[i].StringValue.ToOperator(), rhs);
                i += 2;
            }
            return lhs;
        }

        private static Expression ParseTerm(Match match)
        {
            Ensure.IsTrue<ArgumentException>(match.Name == "term");
            var lhs = ParseFactor(match.Matches[0]);
            for (int i = 1; i < match.Matches.Count;)
            {
                var rhs = ParseFactor(match.Matches[i + 1]);
                lhs = new OperatorNode(lhs, match.Matches[i].StringValue.ToOperator(), rhs);
                i += 2;
            }
            return lhs;
        }

        private static Expression ParseFactor(Match match)
        {
            Ensure.IsTrue<ArgumentException>(match.Name == "factor");
            var child = match.Matches[0];
            switch (child.Name)
            {
                case "expression": return ParseExpression(child);
                case "number": return new ConstantNode(int.Parse(child.StringValue));
                case "identifier": return new VariableNode(child.StringValue);
                default: throw new ScriptingException($"Unexpected token type '{child.Name}'");
            }
        }

        /// <summary>
        /// Create an expression from a parse tree
        /// </summary>
        public static Expression Parse(Match match)
        {
            return ParseExpression(match).Reduce();
        }

        /// <summary>
        /// Return the default value for expressions
        /// </summary>
        public static Expression Default()
        {
            return new ConstantNode(1);
        }
    }
}