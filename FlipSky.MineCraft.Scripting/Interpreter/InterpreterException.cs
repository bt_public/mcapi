﻿using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft.Scripting.Interpreter
{
    /// <summary>
    /// Exception for preprocessor errors
    /// </summary>
    public class InterpreterException : ScriptingException
    {
        /// <summary>
        /// Additional context information if available
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        public InterpreterException() { }

        /// <summary>
        /// Constructor with a message
        /// </summary>
        public InterpreterException(string message) : base(message) { }

        /// <summary>
        /// Constructor with a message and context
        /// </summary>
        public InterpreterException(string message, string context) : base(message)
        {
            Context = context;
        }

        /// <summary>
        /// Constructor with inner exception
        /// </summary>
        public InterpreterException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Constructor with context and inner exception
        /// </summary>
        public InterpreterException(string message, string context, Exception inner) : base(message, inner)
        {
            Context = context;
        }

        /// <summary>
        /// Constructor with serialization information
        /// </summary>
        protected InterpreterException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}