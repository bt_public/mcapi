﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace FlipSky.MineCraft.Scripting.Interpreter
{
    /// <summary>
    /// Represents the context stack for execution operation sequences.
    /// </summary>
    internal class Context : IContext
    {
        //--- State
        private readonly List<Dictionary<string, int>> Stack = new List<Dictionary<string, int>>();
        private readonly Dictionary<string, Func<int>> Functions = new Dictionary<string, Func<int>>();
        private readonly Random RandomGenerator = new Random();

        /// <summary>
        /// Constructor
        /// </summary>
        public Context()
        {
            // Set state and initial context
            Block = Blocks.AIR;
            Location = Location.Zero;
            Stack.Add(new Dictionary<string, int>());
            // Add access to location
            SetFunction("myX", () => Location.X);
            SetFunction("myY", () => Location.Y);
            SetFunction("myZ", () => Location.Z);
            // Add random number support
            SetVariable("RND_MAX", 100);
            SetFunction("RND", () => RandomGenerator.Next(GetVariable("RND_MAX")));
        }

        /// <summary>
        /// Test (and fetch) a named variable
        /// </summary>
        private bool IsVariableDefined(string name, out int value)
        {
            value = 0;
            // Check for functions
            if (Functions.ContainsKey(name))
            {
                value = Functions[name]();
                return true;
            }
            // Check the context stack
            for (int i = 0; i < Stack.Count; i++)
                if (Stack[i].ContainsKey(name))
                {
                    value = Stack[i][name];
                    return true;
                }
            // No such variable
            return false;
        }

        #region Public API
        /// <summary>
        /// The current location
        /// </summary>
        public Location Location { get; set; }

        /// <summary>
        /// The current block
        /// </summary>
        public int Block { get; set; }

        /// <summary>
        /// Enter a new context
        /// </summary>
        public void Push()
        {
            Stack.Insert(0, new Dictionary<string, int>());
        }

        /// <summary>
        /// Exit the current context
        /// </summary>
        public void Pop()
        {
            if (Stack.Count <= 1)
                throw new InterpreterException("Stack underflow", ToString());
            Stack.RemoveAt(0);
        }

        /// <summary>
        /// Set a function. This is a special type of variable that
        /// cannot be overwritten and returns a generated value.
        /// </summary>
        public void SetFunction(string name, Func<int> func)
        {
            if (IsVariableDefined(name, out var _))
                throw new InterpreterException($"Attempting to override a defined variable or function - '{name}'", ToString());
            Functions[name] = func;
        }

        /// <summary>
        /// Set the value of a variable in the current context
        /// </summary>
        public void SetVariable(string name, int value)
        {
            if (Functions.ContainsKey(name))
                throw new InterpreterException($"Attempting to assign a value to function '{name}'", ToString());
            Stack[0][name] = value;
        }

        /// <summary>
        /// Get the value of a variable (or the result of a function) in the
        /// current context
        /// </summary>
        public int GetVariable(string name)
        {
            if (IsVariableDefined(name, out var value))
                return value;
            throw new InterpreterException($"Attempting to read unassigned variable '{name}'", ToString());
        }
        #endregion

        /// <summary>
        /// Return a string representation of the context
        /// </summary>
        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendLine($"Location: {Location}");
            result.AppendLine($"Block   : {Block}");
            // Show function names
            result.Append("Functions: ");
            if (Functions.Count == 0)
                result.AppendLine("(none)");
            else
                result.AppendLine(string.Join(", ", Functions.Keys));
            // Show context frames
            result.AppendLine();
            for (int i = 0; i < Stack.Count; i++)
            {
                result.AppendLine($"Frame #{i} ...");
                if (Stack[i].Count == 0)
                    result.AppendLine("  (empty)");
                else
                {
                    foreach (var k in Stack[i].Keys)
                        result.AppendLine($"  {k}: {Stack[i][k]}");
                }
            }
            // Pass it back
            return result.ToString();
        }
    }
}
