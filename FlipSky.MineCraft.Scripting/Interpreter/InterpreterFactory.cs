using FlipSky.Depends;

namespace FlipSky.MineCraft.Scripting.Interpreter
{
    /// <summary>
    /// Factory class for buildable interpreters
    /// </summary>
    [DefaultImplementation(typeof(IInterpreterFactory), Lifetime.Singleton)]
    internal class InterpreterFactory : IInterpreterFactory
    {
        /// <summary>
        /// Create an interpreter for the given sequence
        /// </summary>
        public IInterpreter Create(Sequence sequence)
        {
            return new ScriptInterpreter(sequence);
        }
    }
}