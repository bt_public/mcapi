using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using FlipSky.MineCraft.Building;

namespace FlipSky.MineCraft.Scripting.Interpreter
{
    /// <summary>
    /// Delegate for bytecode implementations
    /// </summary>
    internal delegate void ByteCodeExecutor(object[] args, IContext context, IBuildBuffer buffer);

    /// <summary>
    /// Implementation of the interpreter
    /// </summary>
    internal class ScriptInterpreter : IInterpreter
    {
        //--- State and mappings
        private readonly Sequence Sequence;
        private readonly Dictionary<ByteCode, ByteCodeExecutor> Implementations;

        public ScriptInterpreter(Sequence sequence)
        {
            Sequence = sequence;
            // Set up implementations
            Implementations = new Dictionary<ByteCode, ByteCodeExecutor>()
            {
                { ByteCode.Move, ImplMove },            // Move relative to current location
                { ByteCode.Goto, ImplGoto },            // Go to absolute co-ordinate
                { ByteCode.Use, ImplUse },              // Select a block to use
                { ByteCode.Exec, ImplExec },            // Execute a procedure
                { ByteCode.Loop, ImplLoop },            // Execute a loop
                { ByteCode.Set, ImplSet },              // Set a variable value
                { ByteCode.Fill, ImplFill },            // Fill a region with the current block
                { ByteCode.ArcPoints, ImplArcPoints },  // Generate points for an arc
                { ByteCode.LinePoints, ImplLinePoints } // Generate points for a line
            };
        }

        #region Execution Helpers
        /// <summary>
        /// Execute a given sequence of operations
        /// </summary>
        private void ExecuteSequence(IReadOnlyList<Operation> operations, IContext context, IBuildBuffer buffer)
        {
            foreach (var op in operations)
            {
                if (!Implementations.TryGetValue(op.ByteCode, out var impl))
                    throw new InterpreterException($"Unimplemented bytecode '{op.ByteCode}'", context.ToString());
                try
                {
                    impl(op.Args, context, buffer);
                }
                catch (InterpreterException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    throw new InterpreterException($"Error processing bytecode '{op.ByteCode}'", context.ToString(), ex);
                }
            }
        }

        /// <summary>
        /// Evaluate all expression in the argument list and return the values
        /// </summary>
        private int[] EvaluateExpressionArguments(IContext context, object[] args, int offset = 0)
        {
            return args.Skip(offset).Select(v => (v as Expressions.Expression).Evaluate(context)).ToArray();
        }

        /// <summary>
        /// Add all the values to the context as argument variables
        /// </summary>
        private void AddArgumentVariables(IContext context, int[] values)
        {
            for (int i = 0; i < values.Length; i++)
                context.SetVariable($"arg{i}", values[i]);
        }
        #endregion

        #region ByteCode Implementations
        /// <summary>
        /// Implements the 'Move' bytecode
        /// </summary>
        private void ImplMove(object[] args, IContext context, IBuildBuffer buffer)
        {
            var values = EvaluateExpressionArguments(context, args);
            context.Location = context.Location.Translate(values[0], values[1], values[2]);
        }

        /// <summary>
        /// Implements the 'Goto' bytecode
        /// </summary>
        private void ImplGoto(object[] args, IContext context, IBuildBuffer buffer)
        {
            var values = EvaluateExpressionArguments(context, args);
            context.Location = new Location(values[0], values[1], values[2]);
        }

        /// <summary>
        /// Implements the 'Use' bytecode
        /// </summary>
        private void ImplUse(object[] args, IContext context, IBuildBuffer buffer)
        {
            context.Block = (int)args[0];
        }

        /// <summary>
        /// Implements the 'Exec' bytecode
        /// </summary>
        private void ImplExec(object[] args, IContext context, IBuildBuffer buffer)
        {
            var values = EvaluateExpressionArguments(context, args, 1);
            context.Push();
            AddArgumentVariables(context, values);
            ExecuteSequence((args[0] as Sequence).Operations, context, buffer);
            context.Pop();
        }

        /// <summary>
        /// Implements the 'Loop' bytecode
        /// </summary>
        private void ImplLoop(object[] args, IContext context, IBuildBuffer buffer)
        {
            var count = (args[1] as Expressions.Expression).Evaluate(context);
            int[] loopVars = new int[2] { 0, count };
            context.Push();
            for (int i = 0; i < count; i++)
            {
                loopVars[0] = i;
                AddArgumentVariables(context, loopVars);
                ExecuteSequence((args[0] as Sequence).Operations, context, buffer);
            }
            context.Pop();
        }

        /// <summary>
        /// Implements the 'Set' bytecode
        /// </summary>
        private void ImplSet(object[] args, IContext context, IBuildBuffer buffer)
        {
            var value = (args[1] as Expressions.Expression).Evaluate(context);
            context.SetVariable(args[0] as string, value);
        }

        /// <summary>
        /// Implements the 'Fill' bytecode
        /// </summary>
        private void ImplFill(object[] args, IContext context, IBuildBuffer buffer)
        {
            var values = EvaluateExpressionArguments(context, args);
            buffer.Fill(
                context.Location,
                new Location(
                    context.Location.X + values[0],
                    context.Location.Y + values[1],
                    context.Location.Z + values[2]
                    ),
                context.Block
                );
        }

        /// <summary>
        /// Implements the 'ArcPoints' bytecode
        /// </summary>
        private void ImplArcPoints(object[] args, IContext context, IBuildBuffer buffer)
        {
            var radius = (args[1] as Expressions.Expression).Evaluate(context);
            context.Push();
            foreach (var location in Generators.Arc(radius))
            {
                AddArgumentVariables(context, new int[2] { location.X, location.Y });
                ExecuteSequence((args[0] as Sequence).Operations, context, buffer);
            }
            context.Pop();
        }

        /// <summary>
        /// Implements the 'LinePoints' bytecode
        /// </summary>
        private void ImplLinePoints(object[] args, IContext context, IBuildBuffer buffer)
        {
            var values = EvaluateExpressionArguments(context, args, 1);
            context.Push();
            foreach (var location in Generators.Line(values[0], values[1]))
            {
                AddArgumentVariables(context, new int[2] { location.X, location.Y });
                ExecuteSequence((args[0] as Sequence).Operations, context, buffer);
            }
            context.Pop();
        }
        #endregion

        #region Public API
        /// <summary>
        /// Execute the code block into the given buffer
        /// </summary>
        public void Execute(IBuildBuffer buffer)
        {
            // Run the sequence
            ExecuteSequence(Sequence.Operations, new Context(), buffer);
        }
        #endregion

        #region String Representation
        /// <summary>
        /// Generate a string representation of the bytecode
        /// </summary>
        public override string ToString()
        {
            return Sequence.ToString();
        }
        #endregion
    }
}