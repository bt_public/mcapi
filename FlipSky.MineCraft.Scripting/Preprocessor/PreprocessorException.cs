﻿using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft.Scripting.Preprocessor
{
    /// <summary>
    /// Exception for preprocessor errors
    /// </summary>
    public class PreprocessorException : ScriptingException
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        public PreprocessorException() { }

        /// <summary>
        /// Constructor with a message
        /// </summary>
        public PreprocessorException(string message) : base(message) { }

        /// <summary>
        /// Constructor with inner exception
        /// </summary>
        public PreprocessorException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Constructor with serialization information
        /// </summary>
        protected PreprocessorException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}