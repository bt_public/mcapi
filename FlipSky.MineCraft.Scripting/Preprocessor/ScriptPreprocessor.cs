using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using FlipSky.Depends;
using FlipSky.Ensures;

namespace FlipSky.MineCraft.Scripting.Preprocessor
{
    /// <summary>
    /// Implementation of the preprocessor
    /// </summary>
    [DefaultImplementation(typeof(IPreprocessor))]
    public class ScriptPreprocessor : IPreprocessor
    {
        /// <summary>
        /// State for the preprocessor
        /// </summary>
        private class PreprocessorState
        {
            /// <summary>
            /// Base path to find modules
            /// </summary>
            public string ModulePath { get; set; }

            /// <summary>
            /// Set of modules already loaded
            /// </summary>
            public ISet<string> Modules { get; set; } = new HashSet<string>();

            /// <summary>
            /// Current defined aliases
            /// </summary>
            public IDictionary<string, string> Aliases { get; set; }
        }

        //--- Constants
        private const string ModulePathName = "modules";
        private readonly Regex MatchToken = new Regex(@"^[a-zA-z][a-zA-Z0-9_]+");
        private readonly Regex MatchAlias = new Regex(@"\${([a-zA-Z][a-zA-Z0-9_]+)}");
        private readonly Regex MatchComment = new Regex(@"//.*$");

        #region Implementation
        /// <summary>
        /// Strip all comments from the line
        /// </summary>
        private string StripComments(string line)
        {
            var match = MatchComment.Match(line);
            if (match.Success)
              return line.Substring(0, match.Index);
            return line;
        }

        /// <summary>
        /// Replace alias insertions
        /// </summary>
        private string ReplaceAliases(string line, IDictionary<string, string> aliases)
        {
            var match = MatchAlias.Match(line);
            while (match.Success)
            {
                // Figure out what to insert
                string insert = "";
                if (aliases.ContainsKey(match.Groups[1].Value))
                    insert = aliases[match.Groups[1].Value];
                // And replace it
                line = line.Substring(0, match.Index) + insert + line.Substring(match.Index + match.Length);
                // Find the next one
                match = MatchAlias.Match(line);
            }
            return line;
        }

        /// <summary>
        /// Get the first token from the line
        /// </summary>
        private string GetToken(string line, out string remaining)
        {
            line = line.TrimStart();
            var match = MatchToken.Match(line);
            if (!match.Success)
                throw new PreprocessorException($"Expected token in line '{line}'");
            // Save the remainder of the line
            remaining = line.Substring(match.Length);
            return match.Value;
        }

        /// <summary>
        /// Preprocess a single line
        /// </summary>
        private string ProcessSingleLine(PreprocessorState state, string line)
        {
            line = StripComments(line);
            line = ReplaceAliases(line, state.Aliases);
            // Look for directives
            if (!line.StartsWith("#"))
              return line + "\n";
            // Contains directives, process them
            var token = GetToken(line.Substring(1), out line);
            // Process alias definition
            if (token == "alias")
            {
                var name = GetToken(line, out line);
                if (!state.Aliases.ContainsKey(name))
                  state.Aliases[name] = line.Trim();
                return string.Empty;
            }
            // Process module import
            if (token == "import")
            {
                var module = GetToken(line, out line);
                if (line.Trim().Length != 0)
                    throw new PreprocessorException("Only one module may be specified.");
                state.Modules.Add(module);
                return ProcessSingleFile(state, Path.Combine(state.ModulePath, module));
            }
            // Unknown directive
            throw new PreprocessorException($"Unknown directive '{token}'.");
        }

        /// <summary>
        /// Process a single file (main or module)
        /// </summary>
        private string ProcessSingleFile(PreprocessorState state, string filename)
        {
            // Add a default extension and make sure it exists
            filename = filename.AddDefaultExtension(FilenameExtensions.ScriptSourceExtension);
            if (!File.Exists(filename))
                throw new PreprocessorException($"No such file '{filename}'");
            // Set up the result
            var result = new StringBuilder();
            foreach (var line in File.ReadAllLines(filename))
                result.Append(ProcessSingleLine(state, line));
            return result.ToString();
        }
        #endregion

        #region Public API
        /// <summary>
        /// Preprocess the given source file and return the raw source.
        /// </summary>
        public string Preprocess(string filename, IDictionary<string, string> aliases)
        {
            Ensure.IsNotNullOrWhiteSpace<PreprocessorException>(filename);
            Ensure.IsNotNull<PreprocessorException>(aliases);
            // Make sure the main file exists and get the module directory
            filename = Path.GetFullPath(filename);
            var state = new PreprocessorState()
            {
                ModulePath = Path.Combine(Path.GetDirectoryName(filename), ModulePathName),
                Aliases = aliases
            };
            // And process it
            return ProcessSingleFile(state, filename);
        }
        #endregion
    }
}