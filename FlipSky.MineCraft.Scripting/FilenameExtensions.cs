﻿using System.IO;

namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Helpers to manipulate filenames
    /// </summary>
    public static class FilenameExtensions
    {
        /// <summary>
        /// Default extension for source files
        /// </summary>
        public const string ScriptSourceExtension = ".mcs";

        /// <summary>
        /// Default extension for bytecode listings
        /// </summary>
        public const string ScriptByteCodeExtension = ".bytecode.txt";

        /// <summary>
        /// Default extension for source listings
        /// </summary>
        public const string ScriptListingExtension = ".listing.txt";

        /// <summary>
        /// Default extension for change list (model) files
        /// </summary>
        public const string ChangeListExtension = ".mcm";

        /// <summary>
        /// Add a default extension if the file does not have one. Optionally force the extension
        /// to be changed.
        /// </summary>
        public static string AddDefaultExtension(this string filename, string extension, bool force = false)
        {
            var directory = Path.GetDirectoryName(filename);
            var basename = Path.GetFileNameWithoutExtension(filename);
            var current = Path.GetExtension(filename);
            if ((current == string.Empty) || force)
                current = extension;
            return Path.Combine(directory, basename + current);
        }
    }
}
