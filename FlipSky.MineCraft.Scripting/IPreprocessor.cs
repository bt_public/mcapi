using System.Collections.Generic;

namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Interface to the preprocessor
    /// </summary>
    public interface IPreprocessor
    {
        /// <summary>
        /// Preprocess the given source file and return the raw source.
        /// </summary>
        string Preprocess(string filename, IDictionary<string, string> aliases);
    }
}