using System;
using System.Runtime.Serialization;

namespace FlipSky.MineCraft.Scripting
{
    public class ScriptingException : MineCraftException
    {
        /// <summary>
        /// Base constructor
        /// </summary>
        public ScriptingException() { }

        /// <summary>
        /// Constructor with a message
        /// </summary>
        public ScriptingException(string message) : base(message) { }

        /// <summary>
        /// Constructor with inner exception
        /// </summary>
        public ScriptingException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// Constructor with serialization information
        /// </summary>
        protected ScriptingException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}