﻿using System.Collections.Generic;
using System.Text;

namespace FlipSky.MineCraft.Scripting
{
    /// <summary>
    /// Represents a sequence of commands
    /// </summary>
    internal class Sequence
    {
        //-- State
        private List<Operation> OperationList = new List<Operation>();

        /// <summary>
        /// Public access to the list of operations in the sequence
        /// </summary>
        public IReadOnlyList<Operation> Operations => OperationList;

        /// <summary>
        /// Emit a new bytecode to the sequence
        /// </summary>
        public void Emit(ByteCode byteCode, params object[] args)
        {
            OperationList.Add(new Operation(byteCode, args));
        }

        /// <summary>
        /// Generate a bytecode listing
        /// </summary>
        public string ToString(string prefix)
        {
            var result = new StringBuilder();
            foreach (var op in Operations)
                result.AppendLine(op.ToString(prefix));
            return result.ToString().TrimEnd();
        }

        /// <summary>
        /// Generate a string representation of the bytecode
        /// </summary>
        public override string ToString()
        {
            return ToString(string.Empty);
        }
    }
}
