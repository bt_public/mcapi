﻿using System.Collections.Generic;
using FlipSky.MineCraft;

namespace McChat
{
    /// <summary>
    /// Persistent storage interface
    /// </summary>
    public interface IStorage
    {
        /// <summary>
        /// Store a named location for a given player
        /// </summary>
        void SaveLocation(string player, string name, Location value);

        /// <summary>
        /// Get a named location for a given player
        /// </summary>
        bool TryGetLocation(string player, string name, out Location location);

        /// <summary>
        /// Get all named locations for a given player
        /// </summary>
        IEnumerable<string> GetLocations(string player);

        /// <summary>
        /// Store an alias for a named player
        /// </summary>
        void SaveAlias(string player, string name, List<string> alias);

        /// <summary>
        /// Get the named alias for a named player
        /// </summary>
        bool TryGetAlias(string player, string name, out List<string> alias);

        /// <summary>
        /// List all saved aliases for a given player
        /// </summary>
        IEnumerable<string> GetAliases(string player);
    }
}
