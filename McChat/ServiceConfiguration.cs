﻿using FlipSky.AppEngine;

namespace McChat
{
    /// <summary>
    /// Configuration for the service
    /// </summary>
    public class ServiceConfiguration : AppEngineConfiguration
    {
        /// <summary>
        /// Hostname to connect to
        /// </summary>
        public string Hostname { get; set; } = "localhost";

        /// <summary>
        /// Port to use for the connection
        /// </summary>
        public int Port { get; set; } = 4711;

        /// <summary>
        /// Configuration for the bot
        /// </summary>
        public Bot.ChatBotConfiguration ChatBot { get; set; } = new Bot.ChatBotConfiguration();

        /// <summary>
        /// Configuration for the scripting extensions
        /// </summary>
        public Script.ScriptConfiguration Scripts { get; set; } = new Script.ScriptConfiguration();
    }
}
