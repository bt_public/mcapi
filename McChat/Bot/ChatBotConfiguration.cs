﻿namespace McChat.Bot
{
    /// <summary>
    /// Configuration for the ChatBot
    /// </summary>
    public class ChatBotConfiguration
    {
        /// <summary>
        /// Display name. Used to trigger the bot and to identify
        /// it's messages.
        /// </summary>
        public string DisplayName { get; set; } = "bot";

        /// <summary>
        /// This message is displayed on start up.
        /// </summary>
        public string WelcomeMessage { get; set; } = "I am online!";

        /// <summary>
        /// Name of the persistent storage file
        /// </summary>
        public string StorageFile { get; set; } = "storage.json";
    }
}
