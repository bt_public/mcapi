﻿using FlipSky.MineCraft;

namespace McChat.Bot
{
    /// <summary>
    /// Storage record for a location (hides Matrix info)
    /// </summary>
    public class StoredLocation
    {
        //--- Co-ordinates
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        /// <summary>
        /// Create from a 'normal' location
        /// </summary>
        public static StoredLocation FromLocation(Location location)
        {
            return new StoredLocation()
            {
                X = location.X,
                Y = location.Y,
                Z = location.Z
            };
        }

        /// <summary>
        /// Convert to a 'normal' location
        /// </summary>
        public Location ToLocation()
        {
            return new Location(X, Y, Z);
        }
    }
}
