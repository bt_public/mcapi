﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using FlipSky.Ensures;
using FlipSky.Depends;
using FlipSky.Logging;
using FlipSky.MineCraft;

namespace McChat.Bot
{
    /// <summary>
    /// The main ChatBot engine
    /// </summary>
    [DefaultImplementation(typeof(IChatBot), Lifetime.Singleton)]
    public class ChatBot : IChatBot
    {
        //--- Constants
        private static string[] EmptyStringArray = new string[0];

        //--- Injections
        private readonly ILogger Log;
        private readonly IMineCraft API;
        private readonly ChatBotConfiguration Config;

        //--- States
        private readonly Dictionary<string, CommandInfo> Commands;
        private readonly IStorage EntityValues;

        /// <summary>
        /// Constructor
        /// </summary>
        public ChatBot(IMineCraft api, ILogger log, ChatBotConfiguration config)
        {
            Log = log;
            API = api;
            Config = config;
            // Initialise state
            Commands = new Dictionary<string, CommandInfo>();
            EntityValues = Storage.Load(Config.StorageFile);
            // Register the standard commands
            RegisterCommand(this);
        }

        /// <summary>
        /// Send a message from the bot
        /// </summary>
        private void BotMessage(string message)
        {
            API.PostMessage($"<{Config.DisplayName}> {message}");
        }

        /// <summary>
        /// Parse the command into the main command and arguments
        /// </summary>
        private bool ParseCommand(string message, out string command, out List<string> args)
        {
            command = string.Empty;
            args = null;
            // Split up the message
            var parts = message.Split(' ').Where(p => p.Length > 0).ToList();
            if (parts[0] != $"@{Config.DisplayName}")
                return false;
            // Split it out and apply aliases
            if (parts.Count > 1)
                command = parts[1].ToLower();
            args = parts.Skip(2).ToList();
            return true;
        }

        /// <summary>
        /// Apply any aliases
        /// </summary>
        private string ApplyAlias(IEntityApi entity, string command, List<string> args, out List<string> nargs)
        {
            nargs = args;
            if (EntityValues.TryGetAlias(entity.Name, command, out var alias))
            {
                command = alias[0];
                nargs = alias.Skip(1).Concat(args).ToList();
            }
            return command;
        }

        /// <summary>
        /// Dispatch the command for the given user
        /// </summary>
        private bool DispatchCommand(IEntityApi entity, string command, List<string> args)
        {
            // Make sure we have a command
            if (string.IsNullOrWhiteSpace(command))
                command = "help";
            if (!Commands.TryGetValue(command, out var info))
                return false;
            // Execute it
            try
            {
                info.Implementation(EntityValues, entity, args);
            }
            catch (Exception ex)
            {
                BotMessage($"Command {command} failed");
                Log.Error(ex, "Exception while processing command '{0}'", command);
            }
            // Done
            return true;
        }

        #region Public API
        /// <summary>
        /// Register a single command
        /// </summary>
        public void RegisterCommand(string name, string description, ChatCommandDelegate impl)
        {
            Ensure.IsNotNullOrWhiteSpace<ArgumentException>(name);
            Ensure.IsNotNull<ArgumentException>(impl);
            // Create the record
            name = name.ToLower();
            if (Commands.ContainsKey(name))
                throw new InvalidOperationException($"Duplicate command - {name}");
            Commands[name] = new CommandInfo(name, description, impl);
        }

        /// <summary>
        /// Register all commands with the ChatCommandAttribute
        /// </summary>
        public void RegisterCommand(object instance)
        {
            Ensure.IsNotNull<ArgumentException>(instance);
            foreach (var method in instance.GetType().GetMethods())
            {
                if (!(method
                    .GetCustomAttributes(false)
                    .Where(a => a.GetType() == typeof(ChatCommandAttribute))
                    .FirstOrDefault() is ChatCommandAttribute attribute))
                    continue;
                // Create the delegate and register the command
                var impl = method.CreateDelegate(typeof(ChatCommandDelegate), instance);
                RegisterCommand(attribute.Name, attribute.Description, impl as ChatCommandDelegate);
            }
        }

        /// <summary>
        /// Run the service
        /// </summary>
        public void Run()
        {
            var events = API.Events;
            API.PostMessage("Starting bot");
            while (true)
            {
                foreach (var message in events.GetChatEvents())
                {
                    if (ParseCommand(message.Message, out var cmd, out var args))
                    {
                        Log.Info($"Received command '{cmd}' from entity {message.EntityId}");
                        // Apply aliases
                        var entity = API.GetEntity(message.EntityId);
                        cmd = ApplyAlias(entity, cmd, args, out var nargs);
                        if (!DispatchCommand(entity, cmd, nargs))
                            API.PostMessage($"Unrecognised command '{cmd}'");
                    }
                }
                Thread.Sleep(2000);
            }
        }
        #endregion

        #region Standard Commands
        /// <summary>
        /// Help system
        /// </summary>
        [ChatCommand("help", "Show available commands")]
        public void CmdHelp(IStorage storage, IEntityApi entity, List<string> args)
        {
            BotMessage("Available commands:");
            foreach (var cmd in Commands.Keys.OrderBy(c => c))
            {
                var info = Commands[cmd];
                BotMessage($"{info.Name} - {info.Description}");
            }
        }

        /// <summary>
        /// Alias management
        /// </summary>
        [ChatCommand("alias", "Define a command alias")]
        public void CmdAlias(IStorage storage, IEntityApi entity, List<string> args)
        {
            if (args.Count == 0)
            {
                // Show defined aliases
                var aliases = storage.GetAliases(entity.Name);
                if (!aliases.Any())
                    BotMessage("No aliases defined.");
                else
                {
                    BotMessage("Current defined aliases:");
                    foreach (var name in aliases)
                    {
                        EntityValues.TryGetAlias(entity.Name, name, out var alias);
                        var definition = string.Join(" ", alias);
                        BotMessage($"{name} = {definition}");
                    }
                }
            }
            else if (args.Count == 1)
            {
                // Remove an alias
                var name = args[0].ToLower();
                EntityValues.SaveAlias(entity.Name, name, null);
                BotMessage($"Removed alias {name}");
            }
            else
            {
                // Define an alias
                var name = args[0].ToLower();
                EntityValues.SaveAlias(entity.Name, name, args.Skip(1).ToList());
                BotMessage($"Defined alias {name}");
            }
        }
        #endregion
    }
}
