﻿namespace McChat.Bot
{
    internal class CommandInfo
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Description of the command
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Implementation of the command
        /// </summary>
        public ChatCommandDelegate Implementation { get; set; }

        public CommandInfo(string name, string description, ChatCommandDelegate impl)
        {
            Name = name;
            Description = string.IsNullOrWhiteSpace(description) ? $"{name} command." : description;
            Implementation = impl;
        }
    }
}
