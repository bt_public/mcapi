﻿using System.Collections.Generic;

namespace McChat.Bot
{
    /// <summary>
    /// Storage record for a single player
    /// </summary>
    public class EntityStorage
    {
        /// <summary>
        /// Locations
        /// </summary>
        public Dictionary<string, StoredLocation> Locations = new Dictionary<string, StoredLocation>();

        /// <summary>
        /// Aliases
        /// </summary>
        public Dictionary<string, List<string>> Aliases = new Dictionary<string, List<string>>();
    }
}
