﻿using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FlipSky.MineCraft;

namespace McChat.Bot
{
    /// <summary>
    /// Storage for entity specific values
    /// </summary>
    public class Storage : IStorage
    {
        //--- State
        private string Filename;
        public Dictionary<string, EntityStorage> Entities = new Dictionary<string, EntityStorage>();

        /// <summary>
        /// Private storage
        /// </summary>
        private Storage()
        {
        }

        #region Helpers
        /// <summary>
        /// Save any changes made to the storage
        /// </summary>
        private void Save()
        {
            File.WriteAllText(
                Filename,
                JsonConvert.SerializeObject(this, Formatting.Indented)
                );
        }

        /// <summary>
        /// Get a player record
        /// </summary>
        private EntityStorage GetPlayerRecord(string player)
        {
            player = player.ToLower();
            if (!Entities.TryGetValue(player, out var value))
            {
                value = new EntityStorage();
                Entities[player] = value;
            }
            return value;
        }

        /// <summary>
        /// Sanitize the name
        /// </summary>
        private string SanitizeName(string name)
        {
            return name.ToLower();
        }
        #endregion

        #region Public API
        /// <summary>
        /// Store a named location for a given player
        /// </summary>
        public void SaveLocation(string player, string name, Location value)
        {
            name = SanitizeName(name);
            var record = GetPlayerRecord(player);
            if (value == null)
                record.Locations.Remove(name);
            else
                record.Locations[name] = StoredLocation.FromLocation(value);
            Save();
        }

        /// <summary>
        /// Get a named location for a given player
        /// </summary>
        public bool TryGetLocation(string player, string name, out Location location)
        {
            location = null;
            name = SanitizeName(name);
            var record = GetPlayerRecord(player);
            if (!record.Locations.ContainsKey(name))
                return false;
            location = record.Locations[name].ToLocation();
            return true;
        }

        /// <summary>
        /// Get all named locations for a given player
        /// </summary>
        public IEnumerable<string> GetLocations(string player)
        {
            var record = GetPlayerRecord(player);
            return record.Locations.Keys;
        }

        /// <summary>
        /// Store an alias for a named player
        /// </summary>
        public void SaveAlias(string player, string name, List<string> value)
        {
            name = SanitizeName(name);
            var record = GetPlayerRecord(player);
            if (value == null)
                record.Aliases.Remove(name);
            else
                record.Aliases[name] = value;
            Save();
        }

        /// <summary>
        /// Get the named alias for a named player
        /// </summary>
        public bool TryGetAlias(string player, string name, out List<string> alias)
        {
            name = SanitizeName(name);
            var record = GetPlayerRecord(player);
            return record.Aliases.TryGetValue(name, out alias);
        }

        /// <summary>
        /// List all saved aliases for a given player
        /// </summary>
        public IEnumerable<string> GetAliases(string player)
        {
            var record = GetPlayerRecord(player);
            return record.Aliases.Keys;
        }
        #endregion

        /// <summary>
        /// Load from file
        /// </summary>
        public static Storage Load(string filename)
        {
            var storage = new Storage();
            if (File.Exists(filename))
                storage = JsonConvert.DeserializeObject<Storage>(File.ReadAllText(filename));
            storage.Filename = filename;
            return storage;
        }
    }
}