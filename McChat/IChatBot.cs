﻿using System.Collections.Generic;
using FlipSky.MineCraft;

namespace McChat
{
    /// <summary>
    /// Delegate for commands
    /// </summary>
    public delegate void ChatCommandDelegate(
        IStorage storage,  // Access to persistent storage
        IEntityApi entity, // Access to the entity that invoked the command
        List<string> args  // Arguments for the command
        );

    /// <summary>
    /// Interface to the chatbot
    /// </summary>
    public interface IChatBot
    {
        /// <summary>
        /// Register a single command
        /// </summary>
        void RegisterCommand(string name, string description, ChatCommandDelegate impl);

        /// <summary>
        /// Register all commands with the ChatCommandAttribute
        /// </summary>
        void RegisterCommand(object instance);

        /// <summary>
        /// Run the service
        /// </summary>
        void Run();
    }
}
