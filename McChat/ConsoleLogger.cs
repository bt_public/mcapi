﻿using System;
using FlipSky.Logging;

namespace McChat
{
    /// <summary>
    /// Very simple logger implementation that just writes to the console
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        /// <summary>
        /// Write the message
        /// </summary>
        public void Write(Severity severity, string message, Exception cause = null)
        {
            Console.WriteLine($"{severity}: {message}");
            if (cause != null)
            {
                Console.WriteLine(cause);
                Console.WriteLine(cause.StackTrace);
            }
        }
    }
}
