﻿using System.Collections.Generic;
using FlipSky.AppEngine;
using FlipSky.MineCraft;
using Newtonsoft.Json.Linq;

namespace McChat.Commands
{
    /// <summary>
    /// Spawn entities
    /// </summary>
    public class EntityCommands : IExtension
    {
        /// <summary>
        /// Configuration the extension
        /// </summary>
        public void Configure(JObject configuration)
        {
            // Nothing to do
        }

        /// <summary>
        /// Initialise the extension
        /// </summary>
        public void Initialise()
        {
            // Nothing to do
        }

        #region Command Implementations
        /// <summary>
        /// Display where the player currently is
        /// </summary>
        [ChatCommand("entity", "Create one or more entities.")]
        public void CmdEntity(IStorage storage, IEntityApi entity, List<string> args)
        {
            if (args.Count == 0)
            {
                entity.PostMessage("Usage: entity type_or_id [ count ]");
                return;
            }
            // Get the entity to create
            if (!int.TryParse(args[0], out var eid))
                if (!Entities.TryParse(args[0], out eid))
                {
                    entity.PostMessage($"Could not interpret entity '{args[0]}'");
                    return;
                }
            // Figure out how many to create
            int count = 1;
            if (args.Count > 1)
                count = int.Parse(args[1]);
            // Now create them
            var transform = entity.GetTransform();
            var location = Location.Zero.Translate(0, 1, 0);
            for (int i = 0; i < count; i++)
            {
                var local = transform.Apply(location);
                entity.SpawnEntity(local, eid);
                location = location.Translate(0, 1, 0);
            }
        }
        #endregion
    }
}
