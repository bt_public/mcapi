﻿using System.Linq;
using System.Collections.Generic;
using FlipSky.AppEngine;
using FlipSky.MineCraft;
using Newtonsoft.Json.Linq;

namespace McChat.Commands
{
    /// <summary>
    /// Chat commands to display, store and go to locations
    /// </summary>
    public class LocationCommands : IExtension
    {
        //--- Constants
        private const string AnonymousMark = "_";

        /// <summary>
        /// Configuration the extension
        /// </summary>
        public void Configure(JObject configuration)
        {
            // Nothing to do
        }

        /// <summary>
        /// Initialise the extension
        /// </summary>
        public void Initialise()
        {
            // Nothing to do
        }

        #region Command Implementations
        /// <summary>
        /// Display where the player currently is
        /// </summary>
        [ChatCommand("where", "Display your current co-ordinates.")]
        public void CmdWhere(IStorage storage, IEntityApi entity, List<string> args)
        {
            var where = entity.GetLocation();
            var heading = entity.GetHeading();
            entity.PostMessage($"{heading} X: {where.X}, Y: {where.Y}, Z: {where.Z}");
        }

        /// <summary>
        /// Store your current location (with optional name)
        /// </summary>
        [ChatCommand("mark", "Save your current co-ordinates.")]
        public void CmdMark(IStorage storage, IEntityApi entity, List<string> args)
        {
            var where = entity.GetLocation();
            storage.SaveLocation(entity.Name, AnonymousMark, where);
            if (args.Count > 0)
            {
                storage.SaveLocation(entity.Name, args[0], where);
                entity.PostMessage($"{args[0]} = X: {where.X}, Y: {where.Y}, Z: {where.Z}");
            }
            else
                entity.PostMessage($"X: {where.X}, Y: {where.Y}, Z: {where.Z}");
        }

        /// <summary>
        /// Calculate distance between current location and previous mark
        /// </summary>
        [ChatCommand("distance", "Show distance from a mark.")]
        public void CmdDistance(IStorage storage, IEntityApi entity, List<string> args)
        {
            var name = args.Count > 0 ? args[0] : AnonymousMark;
            if (!storage.TryGetLocation(entity.Name, name, out var mark))
                entity.PostMessage("No saved mark");
            else
            {
                var where = entity.GetLocation();
                var w = where.X - mark.X;
                var d = where.Y - mark.Y;
                var h = where.Z - mark.Z;
                entity.PostMessage($"W: {w}, D: {d}, H: {h}");
            }
        }

        /// <summary>
        /// Jump to a specific location or previous mark
        /// </summary>
        [ChatCommand("jump", "Jump to stored or specific location.")]
        public void CmdJump(IStorage storage, IEntityApi entity, List<string> args)
        {
            if (args.Count == 0)
            {
                // Display list of locations
                var names = storage.GetLocations(entity.Name).Where(n => n != AnonymousMark).OrderBy(n => n);
                if (!names.Any())
                    entity.PostMessage("No locations defined.");
                else
                {
                    entity.PostMessage("Defined locations:");
                    foreach (var name in names)
                    {
                        storage.TryGetLocation(entity.Name, name, out var location);
                        entity.PostMessage($"{name} = {location.X}, {location.Y}, {location.Z}");
                    }
                }
            }
            else if (args.Count == 1)
            {
                // Jump to named location
                var name = args[0].ToLower().Trim();
                if (!storage.TryGetLocation(entity.Name, name, out var location))
                    entity.PostMessage($"No defined location called '{name}'");
                else
                    entity.SetLocation(location);
            }
            else if (args.Count == 3)
            {
                // Jump to co-ordinates
                var values = args.Select(a => int.Parse(a)).ToList();
                var location = new Location(values[0], values[1], values[2]);
                entity.SetLocation(location);
            }
        }
        #endregion
    }
}
