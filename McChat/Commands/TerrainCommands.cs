﻿using System.Linq;
using System.Collections.Generic;
using FlipSky.AppEngine;
using FlipSky.MineCraft;
using FlipSky.MineCraft.Building;
using Newtonsoft.Json.Linq;

namespace McChat.Commands
{
    /// <summary>
    /// Terrain manipulation commands
    /// </summary>
    public class TerrainCommands : IExtension
    {
        //--- Constants
        private const int DefaultClearBlock = Blocks.AIR;
        private const int DefaultFillBlock = Blocks.DIRT;

        /// <summary>
        /// Configuration the extension
        /// </summary>
        public void Configure(JObject configuration)
        {
            // Nothing to do
        }

        /// <summary>
        /// Initialise the extension
        /// </summary>
        public void Initialise()
        {
            // Nothing to do
        }

        #region Helpers
        /// <summary>
        /// Parse the arguments for the commands
        /// </summary>
        private Location GetArguments(List<string> args, int required, int preferredBlock, out int block)
        {
            block = preferredBlock;
            // Check arguments (width, depth, height is required)
            if ((args.Count != required) && (args.Count != (required + 1)))
                return null;
            // Figure out the block
            if (args.Count == (required + 1))
                if (!int.TryParse(args[required], out block))
                    if (!Blocks.TryParse(args[required], out block))
                        return null;
            // Get the dimensions
            var dimensions = args.Take(required).Select(d => int.Parse(d)).ToList();
            while (dimensions.Count < 3)
                dimensions.Add(0);
            return new Location(dimensions[0], dimensions[1], dimensions[2]);
        }
        #endregion

        #region Command Implementations
        /// <summary>
        /// Clear a region of terrain to the players level
        /// </summary>
        [ChatCommand("clear", "Clear a region of terrain.")]
        public void CmdClear(IStorage storage, IEntityApi entity, List<string> args)
        {
            var dimensions = GetArguments(args, 2, DefaultClearBlock, out var block);
            if (dimensions == null)
                entity.PostMessage("Usage: clear width depth [block_name_or_id]");
            // Get the transform to use and the target height
            var transform = entity.GetTransform();
            var target = entity.GetLocation().Z - 1;
            // Now walk through the region and adjust the height
            entity.PostMessage($"Clearing to height {target}");
            foreach (var loc in Generators.Region(Location.Zero, dimensions.Translate(0, 0, 1)))
            {
                var local = transform.Apply(loc);
                var height = entity.GetHeight(local);
                while (height.Z > target)
                {
                    entity.SetBlock(height, block);
                    height = height.MoveUp(-1);
                }
            }
        }

        /// <summary>
        /// Fill a region with a specific block
        /// </summary>
        [ChatCommand("fill", "Fill a region of terrain.")]
        public void CmdFill(IStorage storage, IEntityApi entity, List<string> args)
        {
            var dimensions = GetArguments(args, 3, DefaultFillBlock, out var block);
            if (dimensions == null)
                entity.PostMessage("Usage: fill width depth height [block_name_or_id]");
            // Get the transform and apply the changes
            var transform = entity.GetTransform();
            foreach (var loc in Generators.Region(Location.Zero, dimensions))
                entity.SetBlock(transform.Apply(loc), block);
        }
        #endregion
    }
}
