﻿namespace McChat.Script
{
    /// <summary>
    /// Configuration for the scripting commands
    /// </summary>
    public class ScriptConfiguration
    {
        /// <summary>
        /// Location of the scripts directory
        /// </summary>
        public string ScriptDirectory { get; set; } = "../../scripts";
    }
}
