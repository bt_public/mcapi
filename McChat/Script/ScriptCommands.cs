﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using FlipSky.Logging;
using FlipSky.AppEngine;
using FlipSky.MineCraft;
using FlipSky.MineCraft.Building;
using FlipSky.MineCraft.Scripting;
using FlipSky.MineCraft.Scripting.Interpreter;
using Newtonsoft.Json.Linq;

namespace McChat.Script
{
    /// <summary>
    /// Provide access to running scripts from the bot.
    /// </summary>
    public class ScriptCommands : IExtension
    {
        //--- Injections
        private ScriptConfiguration Config;
        private readonly IPreprocessor Preprocessor;
        private readonly IParser Parser;
        private readonly ILogger Log;

        /// <summary>
        /// Constructor with injections
        /// </summary>
        public ScriptCommands(IPreprocessor preprocessor, IParser parser, ILogger log)
        {
            Preprocessor = preprocessor;
            Parser = parser;
            Log = log;
        }

        /// <summary>
        /// Configure the extension
        /// </summary>
        public void Configure(JObject configuration)
        {
            if (configuration != null)
                Config = configuration.ToObject<ScriptConfiguration>();
            else
                Config = new ScriptConfiguration();
        }

        /// <summary>
        /// Initialise the extension
        /// </summary>
        public void Initialise()
        {
            // Nothing to do
        }

        #region Command implementations
        /// <summary>
        /// List available scripts
        /// </summary>
        private void ListScripts(IEntityApi entity)
        {
            var files = Directory.EnumerateFiles(Config.ScriptDirectory, $"*{FilenameExtensions.ScriptSourceExtension}")
                .Select(n => Path.GetFileNameWithoutExtension(n));
            if (!files.Any())
                entity.PostMessage("No script files found");
            else
                entity.PostMessage(string.Join(", ", files));
        }

        /// <summary>
        /// Run a script with the given arguments
        /// </summary>
        private void RunScript(IEntityApi entity, List<string> args)
        {
            var filename = Path.Combine(Config.ScriptDirectory, args[0]);
            filename = filename.AddDefaultExtension(FilenameExtensions.ScriptSourceExtension);
            // Build the set of aliases
            var aliases = new Dictionary<string, string>();
            for (int i = 1; i < args.Count; i++)
                aliases[$"ARG{i - 1}"] = args[i];
            // Preprocess and compile
            var source = Preprocessor.Preprocess(filename, aliases);
            var interpreter = Parser.Parse(source);
            // Run it
            try
            {
                var result = new BuildBuffer();
                interpreter.Execute(result);
                // And apply it
                result.ChangeList.ApplyTo(entity);
            }
            catch (InterpreterException ex)
            {
                entity.PostMessage($"Failed to interpret script, check logs.");
                if (ex.Context != null)
                    Log.Error(ex, "Interpreter error - {0}", ex.Context);
                else
                    Log.Error(ex, "Intepreter error");
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Command entry point
        /// </summary>
        [ChatCommand("run", "Run a script")]
        public void CmdScript(IStorage storage, IEntityApi entity, List<string> args)
        {
            if (args.Count == 0)
                ListScripts(entity);
            else
            {
                try
                {
                    RunScript(entity, args);
                }
                catch (Exception ex)
                {
                    entity.PostMessage($"Something went wrong running {args[0]} - {ex}");
                    Log.Error(ex, "Error running script");
                }
            }
        }
        #endregion
    }
}
