﻿using System;
using System.IO;
using Newtonsoft.Json;
using FlipSky.Depends;
using FlipSky.Ensures;
using FlipSky.Logging;
using FlipSky.AppEngine;
using FlipSky.MineCraft;

namespace McChat
{
    class Program
    {
        /// <summary>
        /// Logging output
        /// </summary>
        private readonly ILogger Log;

        /// <summary>
        /// Dependency resolver
        /// </summary>
        private readonly IResolver Resolver;

        /// <summary>
        /// Program entry point
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var program = new Program();
            try
            {
                program.Run(args);
            }
            catch (Exception ex)
            {
                program.Log.Fatal(ex, "McChat threw an unhandled exception, terminating - {0}", ex);
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        protected Program()
        {
            Log = new ConsoleLogger();
            Resolver = new Resolver();
            // Initialise injections
            Resolver.Register(Log);
            Resolver.Register(Resolver);
        }

        /// <summary>
        /// Run the application
        /// </summary>
        protected void Run(string[] args)
        {
            // Load the configuration (if available)
            var config = new ServiceConfiguration();
            if (args.Length > 0)
                config = JsonConvert.DeserializeObject<ServiceConfiguration>(File.ReadAllText(args[0]));
            Resolver.Register(config.ChatBot);
            Resolver.Register(config.Scripts);
            // Attempt to connect to the server
            var factory = Resolver.Resolve<IServer>();
            var server = factory.Connect(config.Hostname, config.Port);
            Resolver.Register<IMineCraft>(server);
            // Initialise the engine
            var engine = InitialiseEngine(config);
            // Create the bot instance and run it
            var chatbot = Resolver.Resolve<IChatBot>();
            chatbot.Run();
        }

        /// <summary>
        /// Initialise the AppEngine and load all required classes
        /// </summary>
        /// <param name="resolver"></param>
        /// <param name="config"></param>
        /// <param name="Log"></param>
        /// <returns></returns>
        private AppEngine InitialiseEngine(AppEngineConfiguration config)
        {
            Log.Info("Intialising host container.");
            var engine = new AppEngine(Resolver, config, Log);
            engine.ExtensionLoadedEvent += OnExtensionLoaded;
            engine.Initialise();
            return engine;
        }

        /// <summary>
        /// Manage the newly loaded extension
        /// </summary>
        private void OnExtensionLoaded(AppEngine source, IExtension extension)
        {
            // Register any commands
            var chatbot = Resolver.Resolve<IChatBot>();
            chatbot.RegisterCommand(extension);
        }

    }
}
