﻿using System;

namespace McChat
{
    /// <summary>
    /// Attribute to mark command implementations
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ChatCommandAttribute : Attribute
    {
        /// <summary>
        /// Name of the command
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Description of the command
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ChatCommandAttribute(string name, string description = null)
        {
            Name = name;
            Description = description ?? $"{name} command.";
        }
    }
}
