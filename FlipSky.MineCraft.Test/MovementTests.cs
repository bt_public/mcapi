﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FlipSky.MineCraft.Test
{
    /// <summary>
    /// Tests for simple movements
    /// </summary>
    public class MovementTests
    {
        /// <summary>
        /// Simple one point translation
        /// </summary>
        [Theory]
        [InlineData(0, 0, 1)]
        [InlineData(0, 1, 0)]
        [InlineData(0, 1, 1)]
        [InlineData(1, 0, 0)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 0)]
        [InlineData(1, 1, 1)]
        public void TranslationTest(int dx, int dy, int dz)
        {
            // Test in positive
            var l1 = Location.Zero.Translate(dx, dy, dz);
            var l2 = new Location(dx, dy, dz);
            Assert.Equal(l1, l2);
            // Test in negative
            l1 = Location.Zero.Translate(-dx, -dy, -dz);
            l2 = new Location(-dx, -dy, -dz);
            Assert.Equal(l1, l2);
        }

        /// <summary>
        /// Test moving in a compass heading
        /// </summary>
        [Theory]
        [InlineData(Heading.North, 0, 1, 0)]
        [InlineData(Heading.East, 1, 0, 0)]
        [InlineData(Heading.South, 0, -1, 0)]
        [InlineData(Heading.West, -1, 0, 0)]
        public void MoveInHeadingTest(Heading heading, int x, int y, int z)
        {
            var l1 = Location.Zero.Move(heading, 1);
            var l2 = new Location(x, y, z);
            Assert.Equal(l1, l2);
        }

        /// <summary>
        /// Test upward movement
        /// </summary>
        [Fact]
        public void MoveUpTest()
        {
            var l1 = Location.Zero.MoveUp(1);
            var l2 = new Location(0, 0, 1);
            Assert.Equal(l1, l2);
        }

        /// <summary>
        /// Test rotation of heading
        /// </summary>
        [Theory]
        [InlineData(Heading.North, 1, Heading.East)]
        [InlineData(Heading.East, 1, Heading.South)]
        [InlineData(Heading.South, 1, Heading.West)]
        [InlineData(Heading.West, 1, Heading.North)]
        [InlineData(Heading.North, -1, Heading.West)]
        [InlineData(Heading.East, -1, Heading.North)]
        [InlineData(Heading.South, -1, Heading.East)]
        [InlineData(Heading.West, -1, Heading.South)]
        [InlineData(Heading.North, 2, Heading.South)]
        [InlineData(Heading.North, -2, Heading.South)]
        public void HeadingRotationTest(Heading start, int steps, Heading expected)
        {
            var result = start.Rotate(steps);
            Assert.Equal(result, expected);
        }

        /// <summary>
        /// Test conversion of world to local co-ordinate system
        /// </summary>
        [Theory]
        [InlineData(1, 0, 0, 1, 0, 0)]
        [InlineData(0, 1, 0, 0, 0, 1)]
        [InlineData(0, 0, 1, 0, -1, 0)]
        [InlineData(1, 2, 3, 1, -3, 2)]
        [InlineData(1, 3, -2, 1, 2, 3)]
        public void WorldToLocalTest(int x1, int y1, int z1, int x2, int y2, int z2)
        {
            var l1 = new Location(x1, y1, z1).WorldToLocal();
            var l2 = new Location(x2, y2, z2);
            Assert.Equal(l2, l1);
        }

        /// <summary>
        /// Test conversion of local to world co-ordinate system
        /// </summary>
        [Theory]
        [InlineData(1, 0, 0, 1, 0, 0)]
        [InlineData(0, 1, 0, 0, 0, -1)]
        [InlineData(0, 0, 1, 0, 1, 0)]
        [InlineData(1, 2, 3, 1, 3, -2)]
        [InlineData(1, -3, 2, 1, 2, 3)]
        public void LocalToWorldTest(int x1, int y1, int z1, int x2, int y2, int z2)
        {
            var l1 = new Location(x1, y1, z1).LocalToWorld();
            var l2 = new Location(x2, y2, z2);
            Assert.Equal(l2, l1);
        }

    }
}
