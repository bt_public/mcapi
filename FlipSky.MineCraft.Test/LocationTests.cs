﻿using System;
using Xunit;

namespace FlipSky.MineCraft.Test
{
    /// <summary>
    /// Tests on location operations
    /// </summary>
    public class LocationTests
    {
        /// <summary>
        /// Test comparison against null objects
        /// </summary>
        [Fact]
        public void NullComparisonTest()
        {
            var l1 = Location.Zero;
            Assert.False(l1.Equals(null));
        }

        /// <summary>
        /// Test comparison against other classes
        /// </summary>
        [Fact]
        public void OtherComparisonTest()
        {
            var l1 = Location.Zero;
            Assert.False(l1.Equals(new object()));
        }

        /// <summary>
        /// Test comparison of instances
        /// </summary>
        [Theory]
        [InlineData(1, 0, 0, 1, 0, 0, true)]
        [InlineData(0, 1, 0, 0, 1, 0, true)]
        [InlineData(0, 0, 1, 0, 0, 1, true)]
        [InlineData(1, 2, 3, 1, 2, 3, true)]
        [InlineData(1, 2, 3, 1, 2, 0, false)]
        public void ComparisonTest(int x1, int y1, int z1, int x2, int y2, int z2, bool expected)
        {
            var l1 = new Location(x1, y1, z1);
            var l2 = new Location(x2, y2, z2);
            Assert.Equal(expected, l1.Equals(l2));
            Assert.Equal(expected, l2.Equals(l1));
            Assert.Equal(expected, l1.GetHashCode() == l2.GetHashCode());
        }
    }
}
