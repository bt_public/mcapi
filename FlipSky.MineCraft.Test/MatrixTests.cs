﻿using System;
using System.Linq;
using Xunit;

namespace FlipSky.MineCraft.Test
{
    /// <summary>
    /// Test basic matrix operations
    /// </summary>
    public class MatrixTests
    {
        /// <summary>
        /// Ensure size constraints are applied
        /// </summary>
        [Theory]
        [InlineData(0, 0)]
        [InlineData(0, 1)]
        [InlineData(1, 0)]
        [InlineData(0, -1)]
        [InlineData(-1, 0)]
        [InlineData(-1, -1)]
        public void TestSizeConstraints(int rows, int cols)
        {
            Assert.Throws<ArgumentException>(() => new Matrix(rows, cols));
        }

        /// <summary>
        /// Ensure empty initialiser results in zeros
        /// </summary>
        [Fact]
        public void TestEmptyInitialiser()
        {
            var m = new Matrix(3, 4);
            for (int r = 0; r < m.Rows; r++)
                for (int c = 0; c < m.Cols; c++)
                    Assert.Equal(0.0, m.GetElement(r, c));
        }

        /// <summary>
        /// Test initialisation with data
        /// </summary>
        [Theory]
        [InlineData(3, 4)]
        public void TestValidInitialiser(int row, int col)
        {
            // Set up initial values
            var initialiser = Enumerable.Range(0, row * col).Select(v => (double)v).ToArray();
            var m = new Matrix(row, col, initialiser);
            // Verify
            for (int r = 0; r < row; r++)
                for (int c = 0; c < col; c++)
                    Assert.Equal(initialiser[r * col + c], m.GetElement(r, c));
        }

        /// <summary>
        /// Test initialisation with insufficient or too much data
        /// </summary>
        [Fact]
        public void TestInvalidInitialiser()
        {
            Assert.Throws<ArgumentException>(() => new Matrix(3, 4, 1.0, 2.0, 3.0));
            Assert.Throws<ArgumentException>(() => new Matrix(1, 1, 1.0, 2.0, 3.0));
        }

        /// <summary>
        /// Test equality comparison
        /// </summary>
        [Theory]
        [InlineData(1, 4)]
        [InlineData(4, 1)]
        [InlineData(3, 3)]
        public void TestEquality(int row, int col)
        {
            // Set up initial values
            var initialiser = Enumerable.Range(0, row * col).Select(v => (double)v).ToArray();
            var m1 = new Matrix(row, col, initialiser);
            var m2 = new Matrix(row, col, initialiser);
            Assert.Equal(m1, m1);
            Assert.Equal(m1, m2);
            Assert.Equal(m2, m1);
        }

        /// <summary>
        /// Test initialisation with data
        /// </summary>
        [Theory]
        [InlineData(3, 4)]
        public void TestScalarMultiplication(int row, int col)
        {
            // Set up initial values
            var initialiser = Enumerable.Range(0, row * col).Select(v => (double)v).ToArray();
            var m = new Matrix(row, col, initialiser);
            m = m.Multiply(3.0);
            // Verify
            for (int r = 0; r < row; r++)
                for (int c = 0; c < col; c++)
                    Assert.Equal(initialiser[r * col + c] * 3.0, m.GetElement(r, c));
        }

        /// <summary>
        /// Test multiplication of matrices
        /// </summary>
        [Fact]
        public void TestMatrixMultiplication()
        {
            var m1 = new Matrix(2, 3, 1, 2, 3, 4, 5, 6);
            var m2 = new Matrix(3, 2, 7, 8, 9, 10, 11, 12);
            var result = m1.Multiply(m2);
            var expected = new Matrix(2, 2, 58, 64, 139, 154);
            Assert.Equal(expected, result);
        }

    }
}
