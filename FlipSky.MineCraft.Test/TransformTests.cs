﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FlipSky.MineCraft.Test
{
    /// <summary>
    /// Test transformation operations
    /// </summary>
    public class TransformTests
    {
        /// <summary>
        /// Simple one point translation
        /// </summary>
        [Theory]
        [InlineData(0, 0, 1)]
        [InlineData(0, 1, 0)]
        [InlineData(0, 1, 1)]
        [InlineData(1, 0, 0)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 0)]
        [InlineData(1, 1, 1)]
        public void TranslationTest(int dx, int dy, int dz)
        {
            // Test in positive
            var t1 = Transform.Translate(dx, dy, dz);
            var l1 = t1.Apply(Location.Zero);
            var l2 = new Location(dx, dy, dz);
            Assert.Equal(l1, l2);
            // Test in negative
            t1 = Transform.Translate(-dx, -dy, -dz);
            l1 = t1.Apply(Location.Zero);
            l2 = new Location(-dx, -dy, -dz);
            Assert.Equal(l1, l2);
        }

        /// <summary>
        /// Test scaling transformation
        /// </summary>
        [Theory]
        [InlineData(1, 1, 1, 2.0, 2.0, 2.0)]
        public void ScalingTest(int x, int y, int z, double sx, double sy, double sz)
        {
            var t1 = Transform.Scale(sx, sy, sz);
            var l1 = new Location(x, y, z);
            var l2 = t1.Apply(l1);
            Assert.Equal((int)(x * sx), l2.X);
            Assert.Equal((int)(y * sy), l2.Y);
            Assert.Equal((int)(z * sz), l2.Z);
        }

        /// <summary>
        /// Test rotation around the X axis
        /// </summary>
        [Theory]
        [InlineData(0, 0, 1, 90, 0, 1, 0)]
        [InlineData(0, 0, 1, -90, 0, -1, 0)]
        [InlineData(0, 1, 0, 90, 0, 0, -1)]
        [InlineData(0, 1, 0, -90, 0, 0, 1)]
        [InlineData(1, 0, 0, 90, 1, 0, 0)]
        [InlineData(1, 0, 0, -90, 1, 0, 0)]
        [InlineData(1, 2, 3, 90, 1, 3, -2)]
        [InlineData(1, 2, 3, -90, 1, -3, 2)]
        public void RotateXTest(int x1, int y1, int z1, int angle, int x2, int y2, int z2)
        {
            var t1 = Transform.RotateX(angle);
            var l1 = new Location(x1, y1, z1);
            var expected = new Location(x2, y2, z2);
            var result = t1.Apply(l1);
            Assert.Equal(expected, result);
        }

        /// <summary>
        /// Test rotation around the Y axis
        /// </summary>
        [Theory]
        [InlineData(0, 0, 1, 90, 1, 0, 0)]
        [InlineData(0, 0, 1, -90, -1, 0, 0)]
        [InlineData(0, 1, 0, 90, 0, 1, 0)]
        [InlineData(0, 1, 0, -90, 0, 1, 0)]
        [InlineData(1, 0, 0, 90, 0, 0, -1)]
        [InlineData(1, 0, 0, -90, 0, 0, 1)]
        [InlineData(1, 2, 3, 90, 3, 2, -1)]
        [InlineData(1, 2, 3, -90, -3, 2, 1)]
        public void RotateYTest(int x1, int y1, int z1, int angle, int x2, int y2, int z2)
        {
            var t1 = Transform.RotateY(angle);
            var l1 = new Location(x1, y1, z1);
            var expected = new Location(x2, y2, z2);
            var result = t1.Apply(l1);
            Assert.Equal(expected, result);
        }

        /// <summary>
        /// Test rotation around the Z axis
        /// </summary>
        [Theory]
        [InlineData(0, 0, 1, 90, 0, 0, 1)]
        [InlineData(0, 0, 1, -90, 0, 0, 1)]
        [InlineData(0, 1, 0, 90, 1, 0, 0)]
        [InlineData(0, 1, 0, -90, -1, 0, 0)]
        [InlineData(1, 0, 0, 90, 0, -1, 0)]
        [InlineData(1, 0, 0, -90, 0, 1, 0)]
        [InlineData(1, 2, 3, 90, 2, -1, 3)]
        [InlineData(1, 2, 3, -90, -2, 1, 3)]
        public void RotateZTest(int x1, int y1, int z1, int angle, int x2, int y2, int z2)
        {
            var t1 = Transform.RotateZ(angle);
            var l1 = new Location(x1, y1, z1);
            var expected = new Location(x2, y2, z2);
            var result = t1.Apply(l1);
            Assert.Equal(expected, result);
        }

    }
}
