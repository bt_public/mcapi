# MineCraft API and Tools (.NET Core)

An implementation of the MineCraft API client in C# and a set of tools and utilities that make use of it.

[GPLV3](https://www.gnu.org/licenses/gpl-3.0.en.html) (see [LICENSE.txt](license.txt)).
