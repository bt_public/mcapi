using System;
using System.Collections.Generic;
using NDesk.Options;

namespace McLogo
{
    /// <summary>
    /// Options for the command line utility
    /// </summary>
    public class ScriptOptions
    {
        /// <summary>
        /// Command line options
        /// </summary>
        private OptionSet Options;

        /// <summary>
        /// Generate a listing of the preprocessed file
        /// </summary>
        public string Listing { get; set; } = string.Empty;

        /// <summary>
        /// Generate a list of the bytecode generated
        /// </summary>
        public string ByteCode { get; set; } = string.Empty;

        /// <summary>
        /// Optional name of the output file
        /// </summary>
        public string Output { get; set; }

        /// <summary>
        /// Show help information
        /// </summary>
        public bool Help { get; set; }

        /// <summary>
        /// Aliases defined on the command line
        /// </summary>
        public IDictionary<string, string> Aliases { get; private set; } = new Dictionary<string, string>();

        /// <summary>
        /// Define an aliase
        /// </summary>
        private void AddAlias(string value)
        {
            var index = value.IndexOf('=');
            var name = (index < 0) ? value : value.Substring(0, index);
            value = (index < 0) ? "" : value.Substring(index + 1);
            if (!Aliases.ContainsKey(name))
                Aliases[name] = value;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ScriptOptions()
        {
            Options = new OptionSet()
            {
                { "a|alias=", "Define a preprocessor alias.", (v) => AddAlias(v) },
                { "l|listing:", "Generate a listing file.", (v) => Listing = v },
                { "b|bytecode:", "Generate a listing of the bytecode.", (v) => ByteCode = v },
                { "o|output=", "Specify the output file.", (v) => Output = v },
                { "h|help", "Show help information.", (v) => Help = (v != null) }
            };
        }

        /// <summary>
        /// Parse the options
        /// </summary>
        public List<string> Parse(string[] args)
        {
            return Options.Parse(args);
        }

        /// <summary>
        /// Show help mesage
        /// </summary>
        public void ShowHelp(string message)
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("    " + message);
            Console.WriteLine("Options:");
            Options.WriteOptionDescriptions(Console.Out);
        }

    }
}
