﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using FlipSky.Logging;
using FlipSky.Depends;
using FlipSky.MineCraft;
using FlipSky.MineCraft.Building;
using FlipSky.MineCraft.Scripting;
using FlipSky.MineCraft.Scripting.Interpreter;

namespace McLogo
{
    class Program
    {
        /// <summary>
        /// Logging output
        /// </summary>
        private readonly ILogger Log;

        /// <summary>
        /// Dependency resolver
        /// </summary>
        private readonly IResolver Resolver;

        /// <summary>
        /// Command line options
        /// </summary>
        private readonly ScriptOptions Options;

        static void Main(string[] args)
        {
            var program = new Program();
            try
            {
                program.Run(args);
            }
            catch (InterpreterException ex)
            {
                // Add context to message if available
                var message = ex.Message;
                if (ex.Context != null)
                    message = $"{message}\n--- Context Information ---\n{ex.Context}";
                // Show inner exception stack trace if provided
                if (ex.InnerException != null)
                    program.Log.Fatal(ex.InnerException, message);
                else
                    program.Log.Fatal(message);
                Environment.Exit(1);
            }
            /*
                        catch (MineCraftException ex)
                        {
                            if (ex.InnerException != null)
                                program.Log.Fatal(ex.InnerException, ex.Message);
                            else
                                program.Log.Fatal(ex.Message);
                            Environment.Exit(1);
                        }
            */
            catch (Exception ex)
            {
                program.Log.Fatal(ex, "McLogo threw an unhandled exception, terminating - {0}", ex);
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        protected Program()
        {
            Log = new ConsoleLogger();
            Resolver = new Resolver();
            Options = new ScriptOptions();
            // Initialise injections
            Resolver.Register(Log);
            Resolver.Register(Resolver);
        }

        /// <summary>
        /// Generate an arbitrary listing
        /// </summary>
        private void GenerateListing(string filename, string content)
        {
            Console.WriteLine($"Generating listing in file '{filename}'");
            var lines = Regex.Split(content, @"\r\n|\r|\n");
            for (int i = 0; i<lines.Length; i++)
                lines[i] = $"{i.ToString("D4")}: {lines[i]}";
            File.WriteAllLines(filename, lines);
        }

        /// <summary>
        /// Generate a listing of the preprocessed source
        /// </summary>
        private void ListSource(string filename, string source)
        {
            GenerateListing(FilenameExtensions.AddDefaultExtension(filename, FilenameExtensions.ScriptListingExtension, true), source);
        }

        /// <summary>
        /// Generate a listing of the generated bytecode
        /// </summary>
        private void ListBytecode(string filename, string bytecode)
        {
            GenerateListing(FilenameExtensions.AddDefaultExtension(filename, FilenameExtensions.ScriptByteCodeExtension, true), bytecode);
        }

        /// <summary>
        /// Run the application
        /// </summary>
        protected void Run(string[] args)
        {
            // Process command line arguments
            var files = Options.Parse(args);
            if (Options.Help)
            {
                Options.ShowHelp("mclogo [options] filename");
                Environment.Exit(0);
            }
            // Check files
            if (files.Count != 1)
            {
                Log.Error("No file specified on command line.");
                Environment.Exit(1);
            }
            /// Preprocess the source file
            var preprocessor = Resolver.Resolve<IPreprocessor>();
            var source = preprocessor.Preprocess(files[0], Options.Aliases);
            if (Options.Listing != string.Empty)
                ListSource(Options.Listing ?? files[0], source);
            // Parse and create the code
            var parser = Resolver.Resolve<IParser>();
            var code = parser.Parse(source);
            if (Options.ByteCode != string.Empty)
                ListBytecode(Options.ByteCode ?? files[0], code.ToString());
            // Execute
            var buffer = new BuildBuffer();
            code.Execute(buffer);
            Console.WriteLine($"Build space: {buffer.BottomLeft} -> {buffer.TopRight}");
            //Console.WriteLine(buffer.HeightMap());
            //code.Execute();
        }

    }
}
